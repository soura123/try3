import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, Text, TextInput, Button, TouchableOpacity, Image } from 'react-native';
import { colors } from '../utils/constants/colors';
import { fonts } from '../utils/constants/fonts';
import BorderedButton from '../components/BorderButton';
import FormElement, { FORM_INPUT_TYPE_PASSWORD, FORM_INPUT_TYPE_EMAIL, FORM_INPUT_TYPE_TEXT } from "../components/FormElement";
import Header from '../components/header'
import CommonBackground from '../components/CommonBackground';
import { Tabs, Tab } from 'native-base'
import { images } from '../utils/constants/assets'
const styles = StyleSheet.create({
    textfamily: {
        fontFamily: fonts.primary_semi_bold_font
    },
    tabStyle: {
        backgroundColor: colors.primary_background_color,
    },
    textStyle: {
        fontFamily: fonts.primary_bold_font,
        color: colors.tab_text,
        fontSize: 15,
        textAlign: 'center',
    },
    activeTabStyle: {
        backgroundColor: colors.primary_background_color,
    },
    activeTextStyle: {
        fontFamily: fonts.primary_bold_font,
        color: colors.active_tab_text,
        fontSize: 15,
        textAlign: 'center',
    },
    parking_spot: {
        backgroundColor: colors.theme_primary_color,
        height: 50,
        marginVertical: 25,
        marginHorizontal: 4,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingHorizontal: 20,
    },
    btnTextStyle: {
        fontSize: 17,
        fontFamily: fonts.primary_semi_bold_font,
        color: colors.text_color,
    }
})
class Contacts extends Component {

    constructor(props) {
        super(props);
    }


    render() {

        return (
            <CommonBackground backIcon="true" search={true} sort={true} headerTitle="Browse" >

                <View style={[{ flex: 1, justifyContent:'center',alignItems:'center' }]}>
                <Text>To be implemented</Text>
                </View>
            </CommonBackground>
        )
    }
}

export default Contacts;

