// import React, {Component} from 'react';
// import {View, Text, TouchableOpacity, Image} from 'react-native';
// import {images} from '../utils/constants/assets'


// class DashBoardComponnet extends Component{
//     constructor(props){
//         super(props);

//     }

//     render(){
//         return(
//             <View style={[{flex:1, justifyContent:'center', alignItems:'center', marginHorizontal:25}]}>
//                 <View style={[{backgroundColor:'grey', width:"100%",  height:140,borderRadius:5}]}>
//                 <View style={[{margin:15}]}>
//                 <View style={{ borderRadius: 200, width: 60, height: 60, justifyContent: 'center', alignItems: 'center' }}>
//                             <Image source={images.user} style={{ width: 60, height: 60, }} />
//                         </View>

//                 </View>

//                     </View>



//             </View>
//         )
//     }
// }

// export default DashBoardComponnet;

































import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, Text } from 'react-native';
import { colors } from '../utils/constants/colors';
import { fonts } from '../utils/constants/fonts';
import BorderedButton from '../components/BorderButton';
import FormElement, { FORM_INPUT_TYPE_PASSWORD, FORM_INPUT_TYPE_EMAIL } from "../components/FormElement";
import CommonBackground from '../components/CommonBackground';
import { navigationConstants } from '../utils/constants/navigationConstants';
import { stringConstants } from '../utils/constants/stringConstants';
import { globalStyles } from '../utils/globalStyles';
import { doSignIn } from '../network/requests';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import ShowToast from "../utils/showToast";

import { goToNextInput, validateForm, generateFormData } from '../utils';

const styles = StyleSheet.create({
    textfamily: {
        fontFamily: fonts.primary_semi_bold_font
    },
    container: { paddingHorizontal: 25, marginTop: 30,marginBottom:60 },
    TERMS_PRIVACY_TEXT: { color: colors.backgroundColor, fontSize: 11, fontFamily: fonts.primary_regular_font },
    forgot: { textAlign: 'center', marginTop: 42, color: colors.backgroundColor, fontFamily: fonts.primary_medium_font, fontSize: 14,  },
    createAccount: {position:'absolute',bottom:40, alignSelf:'center', color: colors.backgroundColor, fontSize: 14, fontFamily: fonts.primary_regular_font,  }
})
class SignIn extends Component {
    constructor(props) {
        super(props);
        this.passwordRef = React.createRef();
        this.state = {
            formData: {
                email: {
                    validations: {
                        isRequired: true,
                        isEmail: true,
                    },
                    title: stringConstants.EMAIL_TITLE,
                    value: '',
                },
                password: {
                    validations: {
                        isRequired: true,
                    },
                    title: stringConstants.PASSWORD_TITLE,
                    value: '',
                },
            }
        }

    }


    updateFormData = (key, value) => {
        let formData = { ...this.state.formData };
        formData[key]['value'] = value;
        this.setState({ formData })
    }

    handleSubmit = () => {
        let isValid = validateForm(this.state.formData);
        if (isValid) {
            let formData = { ...this.state.formData }
            let apiData = generateFormData(formData);
            console.log("api data", apiData);
            this.handleLogin(apiData)
        }
    }

    handleLogin = (apiData) => {
        console.log("inside sing api function");
        this.props.doSignIn({ user: apiData }, (res) => {
            console.log(" after api inside sing api function", res);
            if (res.api_key) {
                console.log("signin successfully");
                this.props.navigation.navigate(navigationConstants.tab_bar)
                ShowToast(stringConstants.toast_success, stringConstants.sigin_success);
            }
            else {
                ShowToast('danger', res.error);
            }
        })
    }

    render() {
        return (
            <CommonBackground scroll={true} headerTitle={stringConstants.login_text} backIcon="true" skip="true" allowClick={() => { this.props.navigation.navigate(navigationConstants.tab_bar) }} onBackPress={() => { this.props.navigation.goBack() }}>

                <View style={[styles.container]}>
                    <View style={[{ marginBottom: 15 }]}>
                        <Text style={[globalStyles.textInputHeading]}>{stringConstants.email_address_text}</Text>
                        <FormElement
                            type={FORM_INPUT_TYPE_EMAIL}
                            returnKeyType={stringConstants.NEXT_TEXT}
                            onReturn={() => goToNextInput('passwordRef', this)}
                            value={"this.state.formData['email'].value"}
                            onChangeText={(txt) => {
                                this.updateFormData('email', txt)
                            }}
                        />
                    </View>
                    <View style={[{ marginBottom: 25 }]}>
                        <Text style={[globalStyles.textInputHeading]}>{stringConstants.PASSWORD_TITLE}</Text>
                        <FormElement
                            type={FORM_INPUT_TYPE_PASSWORD}
                            returnKeyType={stringConstants.DONE_TEXT}
                            ref={this.passwordRef}
                            onReturn={() => {this.handleSubmit() }}
                            value={this.state.formData['password'].value}
                            onChangeText={(txt) => {
                                this.updateFormData('password', txt)
                            }}
                        />
                    </View>
                    <Text style={[styles.TERMS_PRIVACY_TEXT]}>{stringConstants.BY_CLICK_BELOW_AGREE_TEXT}
                        <Text style={[styles.textfamily]} onPress={()=>{this.props.navigation.navigate(navigationConstants.legal_screen, {terms:true})}}>{stringConstants.TERMS_OF_USE_TEXT}</Text>
                        <Text>{stringConstants.AND_READ_THE_TEXT}</Text>
                        <Text style={[styles.textfamily]} onPress={()=>{this.props.navigation.navigate(navigationConstants.legal_screen,{privacy:true})}}>{stringConstants.PRIVACY_STATEMENT_TEXT}</Text>
                    </Text>
                    <BorderedButton shadowHeight={12} title={stringConstants.LOGIN_BTN} onButtonPress={this.handleSubmit} shadow={true} />
                    <Text onPress={() => { this.props.navigation.navigate(navigationConstants.forgot_password) }} style={[styles.forgot]}>{stringConstants.FORGOT_PASSWORD_TEXT}</Text>
                </View>
                <Text style={[styles.createAccount]}>{stringConstants.CREATE_AN_ACCOUNT_TEXT}<Text onPress={() => { this.props.navigation.navigate(navigationConstants.signup_screen) }} style={[{ fontFamily: fonts.primary_bold_font }]}>{stringConstants.SIGNUP_TEXT}</Text></Text>
            </CommonBackground>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            doSignIn
        },
        dispatch,
    )
}

export default connect(null, mapDispatchToProps)(SignIn);


