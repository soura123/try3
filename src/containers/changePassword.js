import React, { Component } from 'react';

import { View, StyleSheet, ScrollView, Text } from 'react-native';
import FormElement, { FORM_INPUT_TYPE_PASSWORD, FORM_INPUT_TYPE_EMAIL } from '../components/FormElement';
import { globalStyles } from '../utils/globalStyles';
import { navigationConstants } from '../utils/constants/navigationConstants';
import { stringConstants } from '../utils/constants/stringConstants';
import { colors } from '../utils/constants/colors';
import { fonts } from '../utils/constants/fonts';
import BorderedButton from '../components/BorderButton';
import { changePassword } from '../network/requests';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import CommonBackground from '../components/CommonBackground';

import ShowToast from "../utils/showToast";

import { goToNextInput, validateForm, generateFormData } from '../utils';

class ChangePassword extends Component {
    constructor(props) {
        super(props);

        this.passwordRef = React.createRef();

        this.passwordRef = React.createRef();
        this.state = {
            formData: {
                old_password: {
                    validations: {
                        isRequired: true,
                        // isPassword: true
                    },
                    title: stringConstants.old_password,
                    value: '',
                },
                password: {
                    validations: {
                        isRequired: true,
                        isPassword: true
                    },
                    title: stringConstants.new_password,
                    value: '',
                },
                // password_confirmation: {
                //     validations: {
                //         isRequired: true,
                //         isPassword: true
                //     },
                //     title: stringConstants.confirm_password,
                //     value: '',
                // },
            }
        }


    }



    updateFormData = (key, value) => {
        let formData = { ...this.state.formData };
        formData[key]['value'] = value;
        this.setState({ formData })
    }

    handleSubmit = () => {
        let isValid = validateForm(this.state.formData);
        if (isValid) {
            formData['password_confirmation'] = {
                value: formData['password']['value'],
                title: "password_confirmation"
            }
            let formData = { ...this.state.formData }
            let apiData = generateFormData(formData);
            console.log("api data", apiData);
            this.handlePassword(apiData)
        }
    }


    handlePassword = (apiData) => {
        console.log("inside sing api function");
        this.props.changePassword(apiData, (res) => {
            console.log(" after api insidepassword api function", res);
            if (res.message == "Password changed") {
                console.log("change password successfully");
                // this.props.navigation.navigate(navigationConstants.pr)
                ShowToast(stringConstants.toast_success, stringConstants.change_password_success);

            }
            else {
                ShowToast('danger', res.error);
            }
        })
    }


    render() {
        return (
            <CommonBackground backIcon={true} onBackPress={() => { this.props.navigation.goBack() }} headerTitle={"Change Password"} removeSkipText={true} >

                <View style={[{ marginHorizontal: 25, marginTop: 30 }]}>

                    <View style={[{ marginBottom: 15 }]}>
                        <Text style={[globalStyles.textInputHeading]}>{stringConstants.old_password}</Text>
                        <FormElement
                            type={FORM_INPUT_TYPE_PASSWORD}
                            returnKeyType={stringConstants.NEXT_TEXT}
                            ref={this.passwordRef}
                            onReturn={() => goToNextInput('passwordRef', this)}
                            value={this.state.formData['old_password'].value}
                            onChangeText={(txt) => {
                                this.updateFormData('old_password', txt)
                            }}

                        />
                    </View>
                    <View style={[{ marginBottom: 15 }]}>
                        <Text style={[globalStyles.textInputHeading]}>{stringConstants.new_password}</Text>
                        <FormElement
                            type={FORM_INPUT_TYPE_PASSWORD}
                            ref={this.passwordRef}
                            returnKeyType={stringConstants.DONE_TEXT}
                            ref={this.passwordRef}
                            onReturn={() => { this.handleSubmit() }}
                            value={this.state.formData['password'].value}
                            onChangeText={(txt) => {
                                this.updateFormData('password', txt)
                            }}

                        />
                    </View>

                    <BorderedButton shadowHeight={15} title={stringConstants.change_password_btn} onButtonPress={this.handleSubmit} shadow={true} />

                </View>

            </CommonBackground>
        )
    }
}

export default connect(null, { changePassword })(ChangePassword);
