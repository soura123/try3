import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, Text, TextInput, Button } from 'react-native';
import { colors } from '../utils/constants/colors';
import { fonts } from '../utils/constants/fonts';
import BorderedButton from '../components/BorderButton';
import CommonBackground from '../components/CommonBackground';
import { goToNextInput, validateForm, generateFormData } from '../utils';
import { goToBack } from '../utils/globalFunction'
import { stringConstants } from '../utils/constants/stringConstants';
import { sendOtp, verifyOtp } from '../network/requests'
import { connect } from 'react-redux';
import ShowToast from '../utils/showToast'
import { navigationConstants } from '../utils/constants/navigationConstants';
import { showAlertDialog } from '../base/DefaultAlertDialog';
let intervalId;
const styles = StyleSheet.create({
	container: { paddingHorizontal: 25, marginTop: 30 },
	textfamily: {
		fontFamily: fonts.primary_semi_bold_font
	},
	otpTextInputCotainerStyle: {
		flexDirection: 'row',
		justifyContent: "center",
	},
	otpTextInputStyle: {
		textAlign: 'center',
		width: 50,
		height: 50,
		borderWidth: 1,
		borderColor: colors.border_color,
		marginHorizontal: 6,
		borderRadius: 5,
		backgroundColor: colors.border_color
	},
	enterCode: { marginBottom: 18, color: colors.backgroundColor, fontFamily: fonts.primary_medium_font, fontSize: 14 },
	bottom: { flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 25, position: 'absolute', bottom: 40 },
	resend: { flex: 1, fontFamily: fonts.primary_bold_font, fontSize: 14, color: colors.backgroundColor, textDecorationLine: 'underline', },
	button: { width: 135, }

})
class MobileVerification extends Component {

	otpKeyNames = ['otp0', 'otp1', 'otp2', 'otp3', 'otp4', 'otp5'];
	listOfTextInputs = {};

	constructor(props) {
		super(props);

		this.otpRef_0 = React.createRef();
		this.otpRef_1 = React.createRef();
		this.otpRef_2 = React.createRef();
		this.otpRef_3 = React.createRef();
		this.otpRef_4 = React.createRef();
		this.otpRef_5 = React.createRef();
		this.state = {
			otp: ['', '', '', '', '', ''],
			count: 30
		};
		this.startInterval();
	}

	componentDidMount() {
		this.sendOtp();
	}

	componentWillUnmount() {
		clearInterval(intervalId)
	}

	startInterval = () => {
		intervalId = setInterval(() => {
			if (this.state.count > 0) {
				this.setState({ count: this.state.count - 1 }); // This effect depends on the `count` state
			} else {
				clearInterval(intervalId)
			}
		}, 1000);
	}

	handleChange = (val, index) => {
		let otp = [...this.state.otp];
		otp[index] = val
		this.setState({ otp }, () => {
			if (index < 5 && val) {
				goToNextInput(`otpRef_${index + 1}`, this)
			}
		})
	}

	handleSubmit = () => {
		let otp = [...this.state.otp];
		let valid = true;
		otp.map(i => {
			if (i && typeof (parseInt(i) == 'number')) {
				return i;
			} else {
				valid = false
			}
		})
		if (!valid) {
			ShowToast(stringConstants.toast_danger, "Code required");
			return
		}
		let apiData = otp.join('');
		this.props.verifyOtp({ "otp": apiData }, (res) => {
			if (res.error) {
				console.log("verify 11111111111", res)
				ShowToast('danger', res.error)
				// this.startInterval();
			} else {
				ShowToast(stringConstants.toast_success, stringConstants.otp_verify)
				this.props.navigation.navigate(navigationConstants.login_screen);

				showAlertDialog("Signup success", "Please verify your email if not done yet, press continue to Login", "Continue", () => { })
			}
		})
	}

	secondsToHms(d) {
		d = Number(d);
		// var h = Math.floor(d / 3600);
		var m = Math.floor(d % 3600 / 60);
		var s = Math.floor(d % 3600 % 60);

		// var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
		var mDisplay = m > 0 ? m : "";
		var sDisplay = s < 10 ? `0${s}` : s;
		return `${mDisplay}${mDisplay ? ':' : ""}${sDisplay}`;
	}


	// sendOtp = () => {
	// 	this.props.sendOtp((response) => {
	// 		if (response.error) {
	// 			// showToast("danger", response.error);
	// 		} else {
	// 			ShowToast(stringConstants.toast_success, `${response.message}`);
	// 		}
	// 	})
	// }

	sendOtp = () => {
		this.props.sendOtp((response) => {
			if (response.error) {
				// showToast("danger", dfdfgdfg);
			} else {
				showToast(stringConstants.toast_success, `${response.message}`);
				this.setState({ count: 300 }, clearInterval(intervalId))
			}
		})
	}

	render() {

		const { otp, count } = this.state;
		return (
			<CommonBackground backIcon={true} onBackPress={() => { this.props.navigation.goBack() }} removeSkipText={true} headerTitle={stringConstants.MOBILE_VERIFICATION_HEADING} >

				<View style={[styles.container]}>
					<Text style={[styles.enterCode]}>Enter your code</Text>
					<View style={styles.otpTextInputCotainerStyle}>
						<TextInput
							style={styles.otpTextInputStyle}
							keyboardType={'numeric'}
							returnKeyType={stringConstants.NEXT_TEXT}
							autoFocus={true}
							maxLength={1}
							ref={this.otpRef_0}
							value={otp[0]}
							onSubmitEditing={() => goToNextInput('otpRef_1', this)}
							onChangeText={(text) => this.handleChange(text, 0)}
						/>
						<TextInput
							style={styles.otpTextInputStyle}
							keyboardType={'numeric'}
							returnKeyType={stringConstants.NEXT_TEXT}
							maxLength={1}
							ref={this.otpRef_1}
							value={otp[1]}
							onSubmitEditing={() => goToNextInput('otpRef_2', this)}
							onChangeText={(text) => this.handleChange(text, 1)}
							onKeyPress={({ nativeEvent }) => {
								goToBack(nativeEvent.key, 'otpRef_0', this)
							}}
						/>
						<TextInput
							style={styles.otpTextInputStyle}
							keyboardType={'numeric'}
							returnKeyType={stringConstants.NEXT_TEXT}
							maxLength={1}
							ref={this.otpRef_2}
							value={otp[2]}
							onSubmitEditing={() => goToNextInput('otpRef_3', this)}
							onChangeText={(text) => this.handleChange(text, 2)}
							onKeyPress={({ nativeEvent }) => {
								goToBack(nativeEvent.key, 'otpRef_1', this)
							}}
						/>
						<TextInput
							style={styles.otpTextInputStyle}
							keyboardType={'numeric'}
							returnKeyType={stringConstants.NEXT_TEXT}
							maxLength={1}
							ref={this.otpRef_3}
							value={otp[3]}
							onSubmitEditing={() => { }}
							onChangeText={(text) => this.handleChange(text, 3)}
							onKeyPress={({ nativeEvent }) => {
								goToBack(nativeEvent.key, 'otpRef_2', this)
							}}
						/>
						<TextInput
							style={styles.otpTextInputStyle}
							keyboardType={'numeric'}
							returnKeyType={stringConstants.NEXT_TEXT}
							maxLength={1}
							ref={this.otpRef_4}
							value={otp[4]}
							onSubmitEditing={() => goToNextInput('otpRef_5', this)}
							onChangeText={(text) => this.handleChange(text, 4)}
							onKeyPress={({ nativeEvent }) => {
								goToBack(nativeEvent.key, 'otpRef_3', this)
							}}
						/>
						<TextInput
							style={styles.otpTextInputStyle}
							keyboardType={'numeric'}
							returnKeyType={stringConstants.DONE_TEXT}
							maxLength={1}
							ref={this.otpRef_5}
							value={otp[5]}
							onSubmitEditing={() => {this.handleSubmit() }}
							onChangeText={(text) => this.handleChange(text, 5)}
							onKeyPress={({ nativeEvent }) => {
								goToBack(nativeEvent.key, 'otpRef_4', this)
							}}
						/>
					</View>
				</View>

				<View style={[styles.bottom]}>
				<Text style={[styles.resend,{ paddingTop: 20 }, { color: count === 0 ? colors.backgroundColor : 'grey' }]} onPress={count === 0 ? this.sendOtp : () => { }}>{stringConstants.RESEND_CODE_TEXT} {count ? 'In' : null} {count ? this.secondsToHms(count):null}</Text>
					<View style={[styles.button]}><BorderedButton small={true} title={stringConstants.NEXT_BTN} titleStyle={true} onButtonPress={() => { this.handleSubmit() }} /></View>
				</View>
			</CommonBackground>
		)
	}
}

export default connect(null, { sendOtp, verifyOtp })(MobileVerification);