import React, { Component } from "react";
import { View, Text, Image, Platform, ImageBackground, StyleSheet } from "react-native";
import { images } from '../utils/constants/assets';
import { colors } from '../utils/constants/colors';
import BorderedButton from '../components/BorderButton';
// import Swiper from 'react-native-swiper';
import { fonts } from '../utils/constants/fonts'
import CommonBackground from '../components/CommonBackground';
import { navigationConstants } from '../utils/constants/navigationConstants';
import { stringConstants } from '../utils/constants/stringConstants'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.backgroundSecondColor,
        paddingHorizontal: 25
    },
})
class LandingScreen extends Component {
    constructor(props) {
        super(props);
    }

    _renderItems = () => {
        let slides = [1, 2, 3, 4, 5];
        return slides.map(i => (<View style={{ flex: 1, justifyContent: "center", alignItems: "center" }} key={i}>
            <Image source={images.onboard} resizeMode="contain" />
            <Text style={{ fontFamily: fonts.primary_regular_font, fontSize: 16, marginHorizontal: 20, textAlign: "center", marginVertical: 60, }}>{stringConstants.get_started_screen_text}.</Text>
        </View>))
    }

    render() {
        return (
            <CommonBackground skip={this.props.navigation.getParam("inapp") ? false:true} allowClick={() => { this.props.navigation.navigate(navigationConstants.login_screen) }}>
                {/* <Swiper
                    showsButtons={false}
                    // showsPagination={false}:
                    dotColor={colors.dotColor}
                    paginationStyle={{ marginBottom: 100 }}
                    activeDotColor={colors.backgroundColor}
                    activeDotStyle={{ height: 8, width: 8, borderRadius: 200 }}
                >
                    {this._renderItems()}
                </Swiper> */}

                <View style={[{ paddingHorizontal: 25, marginBottom: 40 }]}><BorderedButton shadowHeight={5} onButtonPress={() => {this.props.navigation.getParam("inapp") ?  this.props.navigation.goBack():this.props.navigation.navigate(navigationConstants.login_screen) }} title={this.props.navigation.getParam("inapp")? "Back To Setting":stringConstants.get_started} shadow={true} /></View>

            </CommonBackground>
        )
    }
}

export default LandingScreen;