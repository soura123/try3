import React, {Component} from 'react';
import {View, Text, Image,StyleSheet} from 'react-native';
import CommonBackground from '../components/CommonBackground';
import stringConstants from '../utils/constants/stringConstants';
import {fonts }from '../utils/constants/fonts';
import {colors} from '../utils/constants/colors';
import { WebView } from 'react-native-webview';


const styles = StyleSheet.create({
    webViewStyle: {
        flex: 1,
        marginTop:25,
        backgroundColor: colors.white,
        marginHorizontal: 22

    }
});


class Legal extends Component{

    constructor(props){
        super(props);
        this.state={
            text:"<h2>Heading</h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Condimentum ullamcorper luctus diam ornare proin. Viverra egestas potenti nunc habitant nibh tincidunt arcu. Elementum elit tellus senectus a ultrices sed. Mi ullamcorper at enim, sollicitudin ante. Non felis posuere ullamcorper elementum. <h2>Heading</h2>Rhoncus mattis massa commodo maecenas. Vel erat mauris tempor phasellus libero rhoncus. Fringilla ultrices sed malesuada consequat suspendisse. Lorem magna elementum arcu mi sed. Viverra egestas potenti nunc habitant nibh tincidunt arcu. Elementum elit tellus senectus a ultrices sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Condimentum ullamcorper luctus diam ornare proin. Viverra egestas potenti nunc habitant nibh tincidunt arcu. Elementum elit tellus senectus a ultrices sed. Mi ullamcorper at enim, sollicitudin ante. Non felis posuere ullamcorper elementum. Rhoncus mattis massa commodo maecenas. Rhoncus mattis massa commodo maecenas.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Condimentum ullamcorper luctus diam ornare proin. Viverra egestas potenti nunc habitant nibh tincidunt arcu. Elementum elit tellus senectus a ultrices sed. Mi ullamcorper at enim, sollicitudin ante. Non felis posuere ullamcorper elementum. Rhoncus mattis massa commodo maecenas. Rhoncus mattis massa commodo maecenas.<h2>Heading</h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Condimentum ullamcorper luctus diam ornare proin. Viverra egestas potenti nunc habitant nibh tincidunt arcu. Elementum elit tellus senectus a ultrices sed. Mi ullamcorper at enim, sollicitudin ante. Non felis posuere ullamcorper elementum. <h2>Heading</h2>Rhoncus mattis massa commodo maecenas. Vel erat mauris tempor phasellus libero rhoncus. Fringilla ultrices sed malesuada consequat suspendisse. Lorem magna elementum arcu mi sed. Viverra egestas potenti nunc habitant nibh tincidunt arcu. Elementum elit tellus senectus a ultrices sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Condimentum ullamcorper luctus diam ornare proin. Viverra egestas potenti nunc habitant nibh tincidunt arcu. Elementum elit tellus senectus a ultrices sed. Mi ullamcorper at enim, sollicitudin ante. Non felis posuere ullamcorper elementum. Rhoncus mattis massa commodo maecenas. Rhoncus mattis massa commodo maecenas.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Condimentum ullamcorper luctus diam ornare proin. Viverra egestas potenti nunc habitant nibh tincidunt arcu. Elementum elit tellus senectus a ultrices sed. Mi ullamcorper at enim, sollicitudin ante. Non felis posuere ullamcorper elementum. Rhoncus mattis massa commodo maecenas. Rhoncus mattis massa commodo maecenas.<h2>Heading</h2>"

        }

    }

    render(){
        return(
            <CommonBackground onBackPress={() => { this.props.navigation.goBack() }} backIcon={true}> 
                    <Text style={[{marginTop:15,fontSize:25,fontFamily:fonts.secondary_bold_font, color:colors.backgroundColor,marginLeft:25}]}>{this.props.navigation.getParam("terms") ? "Terms of Use":"Privacy Policy"}</Text>
                    <View style={[{marginHorizontal:25, color:'red',border:2,width:200}]}></View>
                    <View style={[{flex:1 , marginHorizontal:5}]}>
                    <WebView
                    style={[styles.webViewStyle,]}
                    scalesPageToFit={true}
                    source={{
                        html: `<body style=${'font-size:35px;color:black; margin:15px '}>${this.state.text}</body>`,
                    }}
                />
                </View>
            </CommonBackground>
        )
    }
}


export default Legal;