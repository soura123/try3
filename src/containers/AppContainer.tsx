import React from 'react';
import { SafeAreaView } from 'react-navigation';
// import SafeAreaView from 'react-native-safe-area-view'
import { globalStyles } from '../utils/globalStyles';
// import SafeAreaProvider from 'react-native-safe-area-context'
import Dummy from './Dummy';
import { Root } from 'native-base'
import Loader from '../components/Loader';


// import WithAlert from '../hoc/withAlertHandler';
// import Loader from '../components/ui/Loader';
// import NetworkDetect from '../../src/components/util/NetworkDetect';
import Navigation from '../navigation';
// import { $styles } from '../utils';
// import UIProvider from '../components/UIProvider';

const AppContainer = (props) => {
    return (
        // <SafeAreaProvider>
        <SafeAreaView style={globalStyles.flex1WithBackground} forceInset={{ bottom: 'never' }}>
              <Root>
                <Navigation />
                {/* <Loader />  */}
            </Root> 
            {/* <Dummy/>  */}
        </SafeAreaView>
        // </SafeAreaProvider>
    );
};
export default AppContainer;