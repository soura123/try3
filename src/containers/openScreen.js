import React, { Component } from "react";
import { View, Text, Image, ImageBackground, StyleSheet } from "react-native";
import { images } from '../utils/constants/assets';
import { colors } from '../utils/constants/colors'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.backgroundColor,
        justifyContent: 'center',
        alignItems: 'center'
    },
})
class Open extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <ImageBackground style={[styles.container]} source={images.background}>
                    <Image style={{ alignSelf: 'center' }} source={images.logo} resizeMode="contain" />
                </ImageBackground>
            </View>
        )
    }
}

export default Open;