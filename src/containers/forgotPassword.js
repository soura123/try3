import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, Text } from 'react-native';
import { colors } from '../utils/constants/colors';
import { fonts } from '../utils/constants/fonts';
import BorderedButton from '../components/BorderButton';
import FormElement, { FORM_INPUT_TYPE_PASSWORD, FORM_INPUT_TYPE_EMAIL } from "../components/FormElement";
import CommonBackground from '../components/CommonBackground';
import { goToNextInput, validateForm, generateFormData } from '../utils';
import { stringConstants } from '../utils/constants/stringConstants';
import { navigationConstants } from '../utils/constants/navigationConstants';
import ShowToast from '../utils/showToast';
import { doResetPassword } from '../network/requests';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

const styles = StyleSheet.create({
    textfamily: {
        fontFamily: fonts.primary_semi_bold_font
    },
    container: { flex: 1, paddingHorizontal: 25, marginTop: 20 },
    forgot: { textAlign: 'center', fontFamily: fonts.secondary_bold_font, fontSize: 20 },
    textStyle: { textAlign: 'center', paddingHorizontal: 25, marginTop: 35, fontFamily: fonts.primary_regular_font, fontSize: 14, color: colors.backgroundColor },
    inputHeading: { marginBottom: 10, fontFamily: fonts.primary_medium_font, fontSize: 14 },
    secondContainer: { marginTop: 45, marginBottom: 50 },

})

class ForgotPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            formData: {
                email: {
                    validations: {
                        isRequired: true,
                        isEmail: true,
                    },
                    title: stringConstants.EMAIL_TITLE,
                    value: '',
                },
            }
        }

    }

    updateFormData = (key, value) => {
        let formData = { ...this.state.formData };
        formData[key]['value'] = value;
        this.setState({ formData })
    }

    handleSubmit = () => {
        let isValid = validateForm(this.state.formData);
        if (isValid) {
            let formData = { ...this.state.formData }
            let apiData = generateFormData(formData);
            this.handleResetPass(apiData)
        }
    }

    handleResetPass = (apiData) => {
        this.props.doResetPassword({ user: apiData }, (res) => {
            if (res) {
                this.props.navigation.navigate(navigationConstants.login_screen);
                ShowToast(stringConstants.toast_success, stringConstants.FORGOT_SUCCESS);
            }
        },
            (error) => {

            }
        )
    }

    render() {
        return (
            <CommonBackground backIcon={true} onBackPress={() => { this.props.navigation.goBack() }}>

                <View style={[styles.container]}>

                    <Text style={[styles.forgot]}>{stringConstants.FORGOT_PASSWORD_HEADING}</Text>
                    <Text style={[styles.textStyle]}>{stringConstants.ENTER_YOUR_EMAIL_BELOW_TEXT}</Text>
                    <View style={[styles.secondContainer]}>
                        <Text style={[styles.inputHeading]}>{stringConstants.email_address_text}</Text>
                        <FormElement
                            type={FORM_INPUT_TYPE_EMAIL}
                            returnKeyType={stringConstants.DONE_TEXT}
                            onReturn={()=>{this.handleSubmit()}}
                            value={this.state.formData['email'].value}
                            onChangeText={(txt) => {
                                this.updateFormData('email', txt)
                            }}
                        />
                    </View>
                    <BorderedButton shadowHeight={15} title={stringConstants.SEND_BTN} onButtonPress={this.handleSubmit} />
                </View>
            </CommonBackground>
        )
    }
}

export default connect(null, { doResetPassword })(ForgotPassword);