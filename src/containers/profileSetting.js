import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native'
import CommonBackground from '../components/CommonBackground';
import { images } from '../utils/constants/assets';
import { fonts } from '../utils/constants/fonts';
import { colors } from '../utils/constants/colors';
import ConfirmationModal from '../components/ConfirmationModal'
import { stringConstants } from '../utils/constants/stringConstants';
import { connect } from 'react-redux';
import { doSignOut } from '../network/requests'
import ShowToast from '../utils/showToast'
import { navigationConstants } from '../utils/constants/navigationConstants';
import { apiConstants } from '../utils/constants/apiConstants'

const styles = StyleSheet.create({
    wrapperView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        backgroundColor: '#E5E5E5',
        borderColor: '#BDBDBD',
        alignItems: 'center',
        paddingVertical: 15,
        paddingHorizontal: 25,
    },
    textStyle: {
        fontSize: 14,
        fontFamily: fonts.primary_medium_font,
        color: colors.backgroundColor
    },
})
class ProfileSetting extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLogout: false,
        }
    }

    handleClicked = (type) => {
        if (type === 'sign_out') {
            this.setState({ isLogout: true })
        }
        if (type === "rating") {
            alert("Rating")
        }
        if (type === "onboarding") {
            this.props.navigation.navigate(navigationConstants.onboarding_screen,{inapp:true});
        }
        if (type === "terms") {
            this.props.navigation.navigate(navigationConstants.legal_screen,{terms:true});
            
        }
        else if (type === "privacy") {
            this.props.navigation.navigate(navigationConstants.legal_screen,{privacy:true})
        } else if (type === "payment") {
            alert("Payment")
        }
    }


    handleSignOut = () => {
        this.setState({
            isLogout: false
        }, () => {
            this.props.doSignOut((res) => {
                ShowToast(stringConstants.toast_success, "Sign Out successfully")
                this.props.navigation.navigate(navigationConstants.onboarding_screen)
            }, (error) => {
                this.props.navigation.navigate(navigationConstants.onboarding_screen)
            })
        })
    }

    render() {
        return (
            <CommonBackground scroll={true} removebackIcon={true} removeSkipText={true} headerTitle={stringConstants.profile}>
                <View style={{ flex: 1, marginTop: 5, }}>
                    <View
                        style={{ paddingLeft: 25, marginVertical: 20, flexDirection: 'row', alignItems: 'center', marginBottom: 30 }}
                    >
                        <View style={{ borderRadius: 200, width: 60, height: 60, justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={images.user} style={{ width: 60, height: 60, }} />
                        </View>
                        <View style={[{ flexDirection: 'column', marginTop: 5 }]}>
                            <Text style={{ marginLeft: 15, color: colors.backgroundColor, fontFamily: fonts.primary_semi_bold_font, fontSize: 16, }}>Harry Styles</Text>
                            <Text style={{ marginTop: 2, marginLeft: 15, color: colors.backgroundColor, fontFamily: fonts.primary_regular_font, fontSize: 14, }}>View Profile</Text>
                        </View>
                        <Image style={[{ position: 'absolute', right: 25, width: 35, height: 35 }]} source={images.bell} />
                    </View>

                    <TouchableOpacity style={[styles.wrapperView, { borderTopWidth: 1, }]} onPress={() => { this.handleClicked('rating') }}>
                        <Text style={styles.textStyle}>{stringConstants.rating_review}</Text>
                        <Image resizeMode="contain" source={images.settingBackIcon} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.wrapperView} onPress={() => this.handleClicked('payment')}>
                        <Text style={styles.textStyle}>{stringConstants.payment_method}</Text>
                        <Image resizeMode="contain" source={images.settingBackIcon} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.wrapperView} onPress={() => { this.props.navigation.navigate(navigationConstants.change_password) }}>
                        <Text style={styles.textStyle}>{stringConstants.change_password}</Text>
                        <Image resizeMode="contain" source={images.settingBackIcon} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.wrapperView} onPress={() => this.handleClicked('onboarding')}>
                        <Text style={styles.textStyle}>{stringConstants.onboarding_tutorial}</Text>
                        <Image resizeMode="contain" source={images.settingBackIcon} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.wrapperView} onPress={() => this.handleClicked('terms')}>
                        <Text style={styles.textStyle}>{stringConstants.term_use}</Text>
                        <Image resizeMode="contain" source={images.settingBackIcon} />
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={1} style={styles.wrapperView} onPress={() => this.handleClicked('privacy')}>
                        <Text style={styles.textStyle}>{stringConstants.privacy_policy}</Text>
                        <Image resizeMode="contain" source={images.settingBackIcon} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.wrapperView} onPress={() => this.handleClicked('sign_out')}>
                        <Text style={styles.textStyle}>{stringConstants.sign_out}</Text>
                        <Image resizeMode="contain" source={images.settingBackIcon} />
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.wrapperView, { borderTopWidth: 1, marginTop: 45, marginBottom: 40 }]}>
                        <Text style={styles.textStyle}>{stringConstants.delete_account}</Text>
                        <Image resizeMode="contain" source={images.settingBackIcon} />
                    </TouchableOpacity>
                </View>
                {this.state.isLogout ?
                    <ConfirmationModal
                        isShow={this.state.isLogout}
                        onClose={() => this.setState({ isLogout: false })}
                        title={stringConstants.sign_out_text}
                        btnText={stringConstants.log_out_heading}
                        description={stringConstants.logout_description_text}
                        onCancel={() => this.setState({ isLogout: false })}
                        onOk={this.handleSignOut}
                    /> : null}
            </CommonBackground>
        )
    }
}

export default connect(null, { doSignOut })(ProfileSetting);