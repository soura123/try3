import React, { Component } from 'react';
import { View, Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { isNotEmpty } from '../utils/globalFunction';
import { apiConstants } from '../utils/constants/apiConstants';
import { navigationConstants } from '../utils/constants/navigationConstants';
import { actionCreators } from "../actions/actionCreators";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { appConstants } from '../utils/constants/appConstants';


class Redirect extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    componentDidMount() {
        this.getAuthToken();
    }


    getAuthToken = () => {
        AsyncStorage.multiGet([apiConstants.user_data_key], (err, stores) => {
            this.storeLocalDataToReducer(stores)
        });
    }

    storeLocalDataToReducer = (data) => {
        data.map((result, i, store) => {
            let key = store[i][0];
            let value = store[i][1];
            switch (key) {
                case apiConstants.user_data_key:
                    this.auth = value;
                    break;
            }
        });
        this.handleNavigation();
    }

    handleNavigation = () => {
        let parsedUserData, parsedAuthData;
        // if (this.userData)
            // parsedUserData = JSON.parse(this.userData)
        if (this.auth)
            parsedAuthData = JSON.parse(this.auth)
        if (isNotEmpty(parsedAuthData)) {
            this.props.auth(parsedAuthData)
            // this.props.user(parsedUserData)
            this.props.navigation.navigate(navigationConstants.tab_bar)
        } else {
            this.props.navigation.navigate(navigationConstants.unauthorized_user_switch)
        }
    }

    render() {
        return <View />
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            auth: actionCreators.fetchUserDetail
        },
        dispatch,
    );
};

export default connect(null, mapDispatchToProps)(Redirect);