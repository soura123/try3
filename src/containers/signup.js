import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, Text,TouchableOpacity,Image } from 'react-native';
import { colors } from '../utils/constants/colors';
import { fonts } from '../utils/constants/fonts';
import BorderedButton from '../components/BorderButton';
import FormElement, { FORM_INPUT_TYPE_PASSWORD, FORM_INPUT_TYPE_EMAIL, FORM_INPUT_TYPE_TEXT, FORM_INPUT_TYPE_NUMBER } from "../components/FormElement";
import CommonBackground from '../components/CommonBackground';
import { navigationConstants } from '../utils/constants/navigationConstants';
import { stringConstants } from '../utils/constants/stringConstants';
import { goToNextInput, validateForm, generateFormData } from '../utils';
import { globalStyles } from '../utils/globalStyles';
import { doSignUp } from '../network/requests';
import { connect } from 'react-redux';
import ShowToast from '../utils/showToast';
import {countryCode} from '../utils/countryJson';
import {images} from '../utils/constants/assets'

import MultiItemPicker from '../components/MultiItemPicker'

const styles = StyleSheet.create({
    textfamily: {
        fontFamily: fonts.primary_semi_bold_font
    },
    container: { paddingHorizontal: 25, marginTop: 30 },
    termsPrivacyText: { color: colors.backgroundColor, fontSize: 11, fontFamily: fonts.primary_regular_font },
    alreadyText: { alignSelf: 'center', marginTop: 50, color: colors.backgroundColor, fontSize: 14, fontFamily: fonts.primary_regular_font, marginBottom: 40 },
    multiSelectStyle: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		borderBottomWidth: 1,
		borderColor: '#E0E0E0',
		marginRight: 7,
        width : '10%',
		paddingHorizontal: 10,
	},
})
class SignUp extends Component {
    constructor(props) {
        super(props);

        // this.firstNameRef = React.createRef();
        this.lastNameRef = React.createRef();
        this.emailRef = React.createRef();
        this.phoneRef = React.createRef();
        this.passwordRef = React.createRef();

        this.state = {

            showSelectScreen: false,
            selectedItems: '+61',
            

            formData: {

                [stringConstants.EMAIL_KEY]: {
                    validations: {
                        isRequired: true,
                        isEmail: true,
                    },
                    title: stringConstants.EMAIL_TITLE,
                    value: '',
                },
                [stringConstants.PASSWORD_KEY]: {
                    validations: {
                        isRequired: true,
                        isPassword: true,
                    },
                    title: stringConstants.PASSWORD_TITLE,
                    value: '',
                },


            },

            user_profile_attributes: {
                [stringConstants.FIRST_NAME_KEY]: {
                    validations: {
                        isRequired: true,
                    },
                    title: stringConstants.FIRST_NAME_TITLE,
                    value: '',
                },
                [stringConstants.LAST_NAME_KEY]: {
                    validations: {
                        isRequired: true,
                    },
                    title: stringConstants.LAST_NAME_TITLE,
                    value: '',
                },
                [stringConstants.MOBILE_NUMBER_KEY]: {
                    validations: {
                        isRequired: true,
                    },
                    title: stringConstants.MOBILE_NUMBER_TITLE,
                    value: '',
                },

            },

        }

    }


    updateFormData = (key, value) => {
        let formData = { ...this.state.formData };
        formData[key]['value'] = value;
        this.setState({ formData })
    }

    updateFormUserData = (key, value) => {

        let formData = { ...this.state.user_profile_attributes };
        formData[key]['value'] = value;
        this.setState({ user_profile_attributes: formData })
    }

    updatePhoneUserData = (key, value) => {
        
        let phonePattern = /^[0-9]*$/;
        if (phonePattern.test(value)) {
            let formData = { ...this.state.user_profile_attributes };
            formData[key]['value'] = value;
            this.setState({ user_profile_attributes: formData })
        }
    }

    handleSubmit = () => {
        let isValid2 = validateForm(this.state.user_profile_attributes);
        let isValid = validateForm(this.state.formData);
        if (isValid && isValid2) {

            
            let formData = { ...this.state.formData }
            formData['password_confirmation'] = {
                value: formData['password']['value'],
                title: "password_confirmation"
            }

            let userData = { ...this.state.user_profile_attributes };

            userData['mobile_number'] = {
                value: this.state.selectedItems + `-` + userData['mobile_number']['value'],
                title: "Mobile Number"
            }

            let apiData = generateFormData(formData);
            let apiData2 = { user_profile_attributes: generateFormData(userData) };

            let api = Object.assign(apiData, apiData2);

            this.props.doSignUp({ user: api }, (res) => {
                if (res.api_key) {
                    ShowToast(stringConstants.toast_success, stringConstants.signup_success);
                    this.props.navigation.navigate(navigationConstants.signup_mobile_number_verification);
                }
                if (Object.keys(res.errors).length > 0) {
                    Object.keys(res.errors).map(i => {
                        ShowToast(stringConstants.toast_danger, `${i} ${res.errors[i]}`)
                    })
                }
            })
        }
    }

    render() {

        const { showSelectScreen, selectedItems } = this.state
        return (
            <CommonBackground scroll={true} backIcon={true} skip={true} headerTitle={stringConstants.CREATE_ACCOUNT_HEADING} onBackPress={() => { this.props.navigation.goBack() }} allowClick={() => { this.props.navigation.navigate(navigationConstants.tab_bar) }}>
                <View style={[styles.container]}>
                    <View style={[{ marginBottom: 15 }]}>
                        <Text style={[globalStyles.textInputHeading]}>{stringConstants.FIRST_NAME_TEXT}</Text>
                        <FormElement
                            type={FORM_INPUT_TYPE_TEXT}
                            returnKeyType={stringConstants.NEXT_TEXT}
                            onReturn={() => goToNextInput('lastNameRef', this)}
                            value={this.state.user_profile_attributes[stringConstants.FIRST_NAME_KEY].value}
                            onChangeText={(txt) => {
                                this.updateFormUserData(stringConstants.FIRST_NAME_KEY, txt)
                            }}
                        />
                    </View>
                    <View style={[{ marginBottom: 15 }]}>
                        <Text style={[globalStyles.textInputHeading]}>{stringConstants.LAST_NAME}</Text>
                        <FormElement
                            type={FORM_INPUT_TYPE_TEXT}
                            ref={this.lastNameRef}
                            returnKeyType={stringConstants.NEXT_TEXT}
                            onReturn={() => goToNextInput('emailRef', this)}
                            value={this.state.user_profile_attributes[stringConstants.LAST_NAME_KEY].value}
                            onChangeText={(txt) => {
                                this.updateFormUserData(stringConstants.LAST_NAME_KEY, txt)
                            }} />
                    </View>
                    <View style={[{ marginBottom: 15 }]}>
                        <Text style={[globalStyles.textInputHeading]}>{stringConstants.email_address_text}</Text>
                        <FormElement
                            type={FORM_INPUT_TYPE_EMAIL}
                            ref={this.emailRef}
                            returnKeyType={stringConstants.NEXT_TEXT}
                            onReturn={() => goToNextInput('passwordRef', this)}
                            value={this.state.formData[stringConstants.EMAIL_KEY].value}
                            onChangeText={(txt) => {
                                this.updateFormData(stringConstants.EMAIL_KEY, txt)
                            }} />
                    </View>
                    <View style={[{ marginBottom: 15 }]}>
                        <Text style={[globalStyles.textInputHeading]}>{stringConstants.CREATE_PASSWORD_TEXT}</Text>
                        <FormElement
                            type={FORM_INPUT_TYPE_PASSWORD}
                            ref={this.passwordRef}
                            returnKeyType={stringConstants.NEXT_TEXT}
                            onReturn={() => goToNextInput('phoneRef', this)}
                            value={this.state.formData[stringConstants.PASSWORD_KEY].value}
                            onChangeText={(txt) => {
                                this.updateFormData(stringConstants.PASSWORD_KEY, txt)
                            }} />
                    </View>
                    <View style={[{ marginBottom: 25 }]}>
                        <Text style={[globalStyles.textInputHeading]}>{stringConstants.MOBILE_NUMBER}</Text>
                        {showSelectScreen &&
								<MultiItemPicker
									isSingle={true}
									title={"country code"}
									items={countryCode ? countryCode : []}
									selectedItems={selectedItems ? selectedItems.split(" ") : []}
									onTap={(data) => {
										if (data.length > 0)
											this.setState({ selectedItems: data.toString() })
									}}
									onClose={() => { this.setState({ showSelectScreen: !showSelectScreen }) }} />
							}
                      <FormElement
                      selectedItems={this.state.selectedItems}
                      onPress={() => { this.setState({ showSelectScreen: !showSelectScreen }) }}
                            type={"phone"}
                            maxLength={14}
                            ref={this.phoneRef}
                            returnKeyType={stringConstants.DONE_TEXT}
                            value={this.state.user_profile_attributes[stringConstants.MOBILE_NUMBER_KEY].value}
                            onChangeText={(txt) => {
                                this.updatePhoneUserData(stringConstants.MOBILE_NUMBER_KEY, txt)
                            }}
                            onReturn={() => { this.handleSubmit() }}
                        /> 
                    </View>
                    <Text style={[styles.termsPrivacyText]}>{stringConstants.BY_CLICK_BELOW_AGREE_TEXT}
                        <Text style={[styles.textfamily]} onPress={()=>{this.props.navigation.navigate(navigationConstants.legal_screen, {terms:true})}}>{stringConstants.TERMS_OF_USE_TEXT}</Text>
                        <Text>{stringConstants.AND_READ_THE_TEXT}</Text>
                        <Text style={[styles.textfamily]} onPress={()=>{this.props.navigation.navigate(navigationConstants.legal_screen, {privacy:true})}}>{stringConstants.PRIVACY_STATEMENT_TEXT}</Text>
                    </Text>
                    <BorderedButton shadowHeight={5} title={stringConstants.SIGN_UP_BTN} onButtonPress={this.handleSubmit} shadow={true} />
                </View>
                <Text style={[styles.alreadyText]}>{stringConstants.ALREADY_ACCOUNT_TEXT}<Text onPress={() => { this.props.navigation.navigate(navigationConstants.login_screen) }} style={[{ fontFamily: fonts.primary_bold_font }]}>{stringConstants.LOGIN_TEXT}</Text></Text>
            </CommonBackground>
        )
    }
}
export default connect(null, { doSignUp })(SignUp);


