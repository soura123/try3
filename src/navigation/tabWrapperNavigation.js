import bottomTabBarNavigation from './bottomTabBarNavigation';
import { createStackNavigator } from 'react-navigation-stack';
import { navigationConstants } from '../utils/constants/navigationConstants';
import LandingScreen from '../containers/onBoard';
import SignIn from '../containers/signin';
import DashBoard from '../containers/dashboard'
import ChangePassword from '../containers/changePassword';
import Legal from '../containers/legal';
// import LandingScreen from '../containers/onBoard';

export default createStackNavigator(
    {
        [navigationConstants.tab_bar]: {
            screen: bottomTabBarNavigation,
        },
        [navigationConstants.onboarding_screen]: {
            screen: LandingScreen,
        },
        [navigationConstants.change_password]: {
            screen: ChangePassword,
        },
        [navigationConstants.legal_screen]: {
			screen: Legal,
        },
        [navigationConstants.onboarding_screen]: {
			screen: LandingScreen,
		},
    },
    {
        headerMode: 'none',
        initialRouteName: navigationConstants.tab_bar,
    },
);
