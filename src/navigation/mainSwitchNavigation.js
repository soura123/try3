import { createSwitchNavigator } from "react-navigation";
import { navigationConstants } from "../utils/constants/navigationConstants";
// import Dummy from "../container/Dummy";
import unauthorizedUser from "./unauthorizedUser";
import tabWrapperNavigation from "./tabWrapperNavigation";
// import Redirect  from '../containers/redirect'


export default createSwitchNavigator({
    // [navigationConstants.redirect_auth]: {
    //     screen: Redirect,
    // },
    [navigationConstants.unauthorized_user_switch]: {
        screen: unauthorizedUser,
    },
    [navigationConstants.tab_bar_wrapper]: {
        screen: tabWrapperNavigation,
    }

}, {
    headerMode: 'none',
    initialRouteName: navigationConstants.unauthorized_user_switch
})