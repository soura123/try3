
import LandingScreen from '../containers/onBoard';
import { navigationConstants } from '../utils/constants/navigationConstants';
import SignIn from '../containers/signin';
import SignUp from '../containers/signup'
import { createStackNavigator } from 'react-navigation-stack';
import ForgotPassword from '../containers/forgotPassword';
import Dummy from '../containers/Dummy';
import mobileVerification from '../containers/mobileVerification';
import Legal from '../containers/legal';

// import DashBoard from '../containers/dashboard'
// import LegalInfo from '../containers/LegalInfo';
// import bottomTabBarNavigation from './bottomTabBarNavigation';

export default createStackNavigator(
	{

		// [navigationConstants.tab_bar]: {
		//     screen: bottomTabBarNavigation,
		// },

		[navigationConstants.onboarding_screen]: {
			screen: LandingScreen,
		},
		[navigationConstants.login_screen]: {
			screen: SignIn,
		},
		[navigationConstants.signup_screen]: {
			screen: SignUp,
		},
		[navigationConstants.forgot_password]: {
			screen: ForgotPassword,
		},
		[navigationConstants.dummy]: {
			screen: Dummy,
		},
		[navigationConstants.signup_mobile_number_verification]: {
			screen: mobileVerification,
		},
		[navigationConstants.legal_screen]: {
			screen: Legal,
		},
		// [navigationConstants.dashboard_screen]: {
		// 	screen: DashBoard,
		// },


	},
	{
		headerMode: 'none',
		initialRouteName: navigationConstants.onboarding_screen,
	},
);
