import { createBottomTabNavigator } from "react-navigation-tabs";
import { Image } from 'react-native'
import React from 'react'
// import Dummy from "../containers/Dummy";
import AsyncStorage from "@react-native-community/async-storage";
import { isNotEmpty } from "../utils/globalFunction";
import { navigationConstants } from '../utils/constants/navigationConstants';
import SignIn from '../containers/signin';
// import SignUp from '../containers/signup'
import Dummy from '../containers/Dummy';
import DashBoard from '../containers/dashboard'
import { images } from '../utils/constants/assets'
import SignUp from "../containers/signup";
import ForgotPassword from "../containers/forgotPassword";
import ProfileSetting from '../containers/profileSetting'
import { stringConstants } from "../utils/constants/stringConstants";
import { apiConstants } from '../utils/constants/apiConstants';
import { showDoubleActionAlertDialog } from "../components/AlertDialog";
import Requests from '../containers/requests';
import Contacts from '../containers/contacts';


const getData = async (props) => {
    let token = await AsyncStorage.getItem(apiConstants.user_data_key)
    if (isNotEmpty(token)) {
        props.defaultHandler();
    } else {
        showDoubleActionAlertDialog(
            stringConstants.signin_required,
            stringConstants.signin_required_desc,
            stringConstants.login_btn_text, () => {
                props.navigation.navigate(navigationConstants.login_screen)
            },
            stringConstants.cancelBtn, () => { }
        )
    }
}

export default createBottomTabNavigator({
    [navigationConstants.DASHBOARD_TAB]: {
        screen: DashBoard,
        navigationOptions: {
            tabBarIcon: ({ focused }) => {
                return focused ?
                    <Image resizeMode="contain" source={images.tab1Active} />
                    :
                    <Image resizeMode="contain" source={images.tab1} />
            },
            tabBarLabel: 'Browse'
        }
    },
    [navigationConstants.OFFERS_TAB]: {
        screen: Requests,
        navigationOptions: {
            // tabBarOnPress: (props) => { alert("yres") },
            tabBarIcon: ({ focused }) => {
                return focused ?
                    <Image resizeMode="contain" source={images.tab2Active} />
                    :
                    <Image resizeMode="contain" source={images.tab2} />
            },
            tabBarLabel: 'Requests'
        }
    },
    [navigationConstants.REWARDS_TAB]: {
        screen: Contacts,
        navigationOptions: {
            // tabBarOnPress: (props) => { alert("yres") },
            tabBarIcon: ({ focused }) => {
                return focused ?
                    <Image resizeMode="contain" source={images.tab3Active} />
                    :
                    <Image resizeMode="contain" source={images.tab3} />
            },
            tabBarLabel: 'Contacts'
        }
    },
    [navigationConstants.PROFILE_TAB]: {

        screen: ProfileSetting,
        navigationOptions: {
            tabBarOnPress: (props) => { getData(props) },
            tabBarIcon: ({ focused }) => {
                return focused ?
                    <Image resizeMode="contain" source={images.tab4Active} />
                    :
                    <Image resizeMode="contain" source={images.tab4} />
            },
            tabBarLabel: 'Profile'
        }
    },
},
    {
        initialRouteName: navigationConstants.DASHBOARD_TAB,
        tabBarOptions: {
            labelStyle: {
                // fontFamily: fontsConst.reg_font,
                // bottom:-10
            },
            tabStyle: {
                paddingBottom: 5,
                paddingTop: 5,
            },
            style: {
                height: 58,
                backgroundColor: '#202020',

            },
            allowFontScaling: true,
            activeTintColor: '#B99255',
            inactiveTintColor: '#FFFFFF',
        },
    }
)