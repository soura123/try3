import React, { Component } from "react";
import { createAppContainer, NavigationContainer } from "react-navigation";
import mainSwitchNavigation from "./mainSwitchNavigation";
import navigationUtils from "../utils/navigationUtils";


class Navigation extends Component {

    constructor(props: Readonly<{}>) {
        super(props)
    }

    render() {
        const AppContainer: NavigationContainer = createAppContainer(mainSwitchNavigation);
        return (<AppContainer ref={(navRef) => navigationUtils.setTopLevelNavigator(navRef)} />)

    }

}

export default Navigation;