import React, { Component } from 'react';
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Picker, Image } from 'react-native';
import FormElement from "../components/FormElement";
import Header from "../components/header";
import MultiItemPicker from '../components/MultiItemPicker'
import { goToNextInput, validateForm, generateFormData } from '../utils';
import CommonBackground from '../components/CommonBackground'
import { images } from "../utils/constants/assets"
import { fonts } from '../utils/constants/fonts';
import { colors } from '../utils/constants/colors';
import FooterBtn from '../components/footerBtn'
import { countryCode } from '../utils/countryJson'
import { connect } from 'react-redux';
import { updateProfile, sendOtp } from '../network/requests'
import showToast from '../utils/showToast'
import { navigationConstants } from '../utils/constants/navigationConstants';
import TextString from '../utils/constants/Strings/TextString';
import { parsePhoneNumberFromString } from 'libphonenumber-js'


class PhoneNumber extends Component {
	constructor(props) {
		super(props)
		this.state = {
			showSelectScreen: false,
			selectedItems: '+61',
			formData: {
				'mobile_number': {
					validations: {
						isRequired: true,
					},
					title: TextString.mobile_number,
					value: '',
				}
			}
		}
	}
	sendOtp = () => {
		this.props.sendOtp((response) => {
			if (response.error) {
				// showToast("danger", response.error);
			} else {
				showToast(TextString.toast_success, `${response.message}`);
				this.props.navigation.navigate(navigationConstants.signup_mobile_number_verification)
			}
		})
	}

	handleSubmit = () => {
		let isValid = validateForm(this.state.formData);
		if (isValid) {
			let formData = { ...this.state.formData }
			let apiData = generateFormData(formData);
			let mobile_number = `${this.state.selectedItems}${apiData.mobile_number}`
			const phoneNumber = parsePhoneNumberFromString(mobile_number);
			if (!phoneNumber.isValid()) {
				showToast("danger", TextString.invalid_phone_number)
				return
			}
			this.props.updateProfile({ 'user_profile[mobile_number]': mobile_number }, (res) => {
				if (res.error) {
					//showToast("danger", res.error);
				} else {
					this.sendOtp();
				}
			})
		}
	}

	updateFormData = (key, value) => {
		let formData = { ...this.state.formData };
		formData[key]['value'] = value;
		this.setState({ formData })
	}

	render() {
		const { showSelectScreen, selectedItems } = this.state
		console.log(selectedItems)
		return (
			<CommonBackground
				skip={false}
				headerTitle={TextString.what_number}
				onBackPress={() => this.props.navigation.goBack()}
				allowClick={() => { }}
				scroll={true}>
				<View style={[{ flex: 1, alignItems: 'center', marginTop: 50 }]}>
					<View style={{ marginHorizontal: 20 }}>
						<Text style={[styles.label]}>{TextString.phone_text}</Text>
						<View style={{ flexDirection: 'row', width: '100%', paddingHorizontal: 15 }}>
							<TouchableOpacity style={[styles.multiSelectStyle]} opacity={0.5} onPress={() => { this.setState({ showSelectScreen: !showSelectScreen }) }}>
								<Text>{selectedItems}</Text>
								<View style={{ width: 9, height: 9, overflow: 'hidden'}}>
									<Image style={{ width: '100%', height: '100%' }} resizeMode="contain" source={images.arrow} />
								</View>
							</TouchableOpacity>
							{showSelectScreen &&
								<MultiItemPicker
									isSingle={true}
									title={TextString.country_code}
									items={countryCode ? countryCode : []}
									selectedItems={selectedItems ? selectedItems.split(" ") : []}
									onTap={(data) => {
										if (data.length > 0)
											this.setState({ selectedItems: data.toString() })
									}}
									onClose={() => { this.setState({ showSelectScreen: !showSelectScreen }) }} />
							}
							<View style={{ flex: 1, }}>
								
								<FormElement
									autoFocus='true'
									type='phone'
									onReturn={() => { }}
									keyboardType="number-pad"
									returnKeyType={TextString.done_text}
									onReturn={() => { }}
									value={this.state.formData['mobile_number'].value}
									onChangeText={(txt) => {
										this.updateFormData('mobile_number', txt)
									}}

								/>
							</View>
						</View>
					</View>
				</View>
				<View style={{ flex: 1, justifyContent: 'flex-end', bottom: 55 }}>
					<View style={{ alignSelf: "flex-end", right: 25 }}>
						<FooterBtn onPress={() => this.handleSubmit()} />
					</View>
				</View>
			</CommonBackground>
		);
	}
}

const styles = StyleSheet.create({
	wrap: {
		flex: 1,
	},
	form: {
		// paddingTop:20,
		marginLeft: 30,
		marginRight: 30
	},
	textAlign: {
		alignContent: 'center',
		margin: 80,
	},
	font12: {
		fontSize: 12,
	},
	setItem: {
		// flexBasis:'35%',
		alignItems: 'center',
		justifyContent: 'center'

	},
	titleStyle: {
		alignSelf: 'flex-start',
		// fontFamily: fontsConst.reg_font,
		marginLeft: 0,
		marginBottom: 8,
		marginTop: 20,
		fontSize: 14,
	},
	multiSelectStyle: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		borderBottomWidth: 1,
		borderColor: '#E0E0E0',
		marginRight: 7,
		width : '18%',
		paddingHorizontal: 10,
	},
	label: {
		fontSize: 14,
		fontFamily: fonts.primary_medium_font,
		color: colors.text_color
	}
});

export default connect(null, { updateProfile, sendOtp })(PhoneNumber);