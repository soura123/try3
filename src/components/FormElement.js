import React, { useState } from 'react';
import { View, TextInput, StyleSheet, Platform, Picker, TouchableOpacity, Text, Image } from 'react-native';
import { colors } from '../utils/constants/colors'
import { fonts } from '../utils/constants/fonts';
import { Icon } from 'native-base';
import iconsConst from '../../src/utils/constants/iconsConst'
import {images} from '../utils/constants/assets';

export const FORM_INPUT_TYPE_TEXT = 'text';
export const FORM_INPUT_TYPE_PASSWORD = 'password';
export const FORM_INPUT_TYPE_RADIO = 'radio';
export const FORM_INPUT_TYPE_CHECK = 'check';
export const FORM_INPUT_TYPE_PICKER = 'picker';
export const FORM_INPUT_TYPE_NUMBER = 'numeric';
export const FORM_INPUT_TYPE_EMAIL = 'email';
export const FORM_INPUT_TYPE_WITH_BORDER = 'border_input';


const styles = StyleSheet.create({
	inputWrapper: {

		flexDirection: 'row',
		borderWidth: 1,
		borderColor: '#f2f2f2',
		borderRadius: 8,
		width: '100%',
		alignItems: 'center',
		minHeight: 45,
	},
	no_border: {
		borderBottomWidth: 0,
	},
	input: {
		paddingLeft: 10,
		color: "black",
		borderTopRightRadius: 5,
		borderBottomRightRadius:5,
		backgroundColor: '#F2F2F2',
		flex: 1,
		fontSize: 14,
		fontFamily: fonts.primary_medium_font,
		minHeight: 45,

	},
	borderedInputWrapper: {
		borderRadius: 5,
		flexDirection: 'row',
		borderWidth: 1,
		borderColor: colors.theme_input_border,
		width: '100%',
		alignItems: 'center',
		minHeight: 45,
	},
	inputIcon: {
		marginRight: 5,
	},
	inputWrapperIos: {
		minHeight: 45,
	},
	placeholder: {
		fontSize: 14,
		// fontFamily:fontsConst.med_font,
	},
	border: {
		borderWidth: 1,
		borderColor: colors.theme_input_border,
		paddingHorizontal: 10
	},
	multiSelectStyle: {
		flexDirection: 'row',
		// justifyContent: 'space-between',
		alignItems: 'center',
		borderTopLeftRadius: 5,
		borderBottomLeftRadius:5,
		// borderBottomWidth: 1,
		borderColor: '#E0E0E0',
		// marginRight: 7,
		height:48,
        width:65,
		// paddingHorizontal: 10,
	},
});

export default React.forwardRef((props, ref) => {
	const { type, icon, placeholder, returnKeyType, onReturn, onChangeText, value, maxLength, keyboardType, topLabelText, topLabelTextStyle } = props;
	const [secret, setSecret] = useState(true);



	switch (type) {
		case FORM_INPUT_TYPE_TEXT: {
			return (
				<View
					style={[styles.inputWrapper, Platform.OS === 'ios' && styles.inputWrapperIos, props.border ? styles.border : null]}
				>
					{icon && <Icon name={icon} style={styles.inputIcon}></Icon>}
					<TextInput

						placeholder={placeholder}
						// placeholderTextColor=}
						placeholderStyle={[styles.placeholder]}
                        blurOnSubmit={false}
						style={[styles.input]}
						placeholder={placeholder}
						// placeholderTextColor={colors.theme_place_color}
						returnKeyType={returnKeyType}
						ref={ref}
						value={value}
						onSubmitEditing={onReturn}
						onChangeText={onChangeText}
					/>
				</View>
			);
		}
		case FORM_INPUT_TYPE_EMAIL: {
			return (
				<View
					style={[styles.inputWrapper, Platform.OS === 'ios' && styles.inputWrapperIos, props.border ? styles.border : null]}
				>
					<TextInput
						style={[styles.input]}
						placeholder={placeholder}
						// placeholderTextColor={colors.theme_place_color}
						placeholderStyle={{ marginHorizontal: -10, }}
						textContentType='emailAddress'
						keyboardType='email-address'
						placeholderStyle={[, styles.placeholder]}
						returnKeyType={returnKeyType}
						ref={ref}
						value={value}
						autoCapitalize={'none'}
						blurOnSubmit={false}
						// autoCompleteType={'off'}
						onSubmitEditing={onReturn}
						onChangeText={onChangeText}
					/>
				</View>
			);
		}
		case FORM_INPUT_TYPE_PASSWORD: {
			return (
				<View
					style={[styles.inputWrapper, Platform.OS === 'ios' && styles.inputWrapperIos, props.border ? styles.border : null,{backgroundColor:'#f2f2f2'}]}
				>
					<TextInput
						style={[, styles.input]}
						ref={ref}
						secureTextEntry={secret}
						placeholder={placeholder}
						placeholderStyle={[, styles.placeholder]}
						placeholderTextColor={colors.theme_place_color}
						placeholderStyle={[, styles.placeholder]}
						returnKeyType={returnKeyType}
						blurOnSubmit={false}
						ref={ref}
						autoCapitalize={'none'}
						value={value}
						onSubmitEditing={onReturn}
						onChangeText={onChangeText}
					/>
					{!secret ? (
						<View style={[{marginRight:10}]}><Icon
						style={[{color:'grey'}]}
							onPress={() => setSecret(true)}
							active
							name={iconsConst.eyeOn}
							type="MaterialIcons"
						></Icon></View>
					) : (
							<View style={[{marginRight:10}]}><Icon
								onPress={() => setSecret(false)}
								active
								name={iconsConst.eyeOff}
								type="MaterialIcons"
							></Icon></View>
						)}
				</View>
			);
		}
		// case 'radio': {
		// 	return (
		// 		<Radio
		// 			text={placeholder}
		// 			radioContainerStyle={props.style}
		// 			selected={props.selected === value}
		// 			onPress={() => props.onChange(value)}
		// 		/>
		// 	);
		// }
		case 'phone': {
			return (
				<View
					style={[styles.inputWrapper, Platform.OS === 'ios' && styles.inputWrapperIos && styles.inputWrapperIos, props.border ? styles.border : null]}
				>
					{/* {icon && <Icon name={icon} style={styles.inputIcon}></Icon>} */}
					<TouchableOpacity style={[styles.multiSelectStyle, {backgroundColor:'#F2F2F2'}]} opacity={0.5} onPress={props.onPress}>
								<Text style={[{marginLeft:8}]}>{props.selectedItems}</Text>
								<View style={{ width: 12, height: 12,marginLeft:4,overflow: 'hidden'}}>
									<Image style={{ width: '100%', height: '100%', }} resizeMode="contain" source={images.arrow} />
								</View>
							</TouchableOpacity>
					<TextInput
						//editable={disable ? false : true}
						// disabled={true}
						placeholderTextColor={colors.theme_place_color}
						maxLength={maxLength}
						scrollEnabled={false}
						style={[styles.input]}
						placeholder={placeholder}
						returnKeyType={returnKeyType}
						ref={ref}
						blurOnSubmit={false}
						value={value}
						keyboardType="phone-pad"
						onSubmitEditing={onReturn}
						onChangeText={onChangeText}
					/>
				</View>
			);
		}
		case FORM_INPUT_TYPE_WITH_BORDER: {
			return (
				<View>
					{topLabelText ?
						<Text style={[topLabelTextStyle, { color: colors.theme_place_color, fontFamily: fonts.primary_semi_bold_font, fontSize: 14, marginBottom: 8 }]}>
							{topLabelText}
						</Text> : null}
					<View style={[styles.borderedInputWrapper, Platform.OS === 'ios' && styles.inputWrapperIos && styles.inputWrapperIos, props.border ? styles.border : null]}>
						<TextInput
							placeholderTextColor={colors.theme_place_color}
							maxLength={maxLength}
							scrollEnabled={false}
							style={[styles.input, { marginLeft: 5 }]}
							placeholder={placeholder}
							returnKeyType={returnKeyType}
							ref={ref}
							value={value}
							keyboardType={keyboardType}
							onSubmitEditing={onReturn}
							onChangeText={onChangeText}
						/>
					</View>
				</View>
			);
		}
		default: {
			return null;
		}
	}

});
