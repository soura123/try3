import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import CommonModal from './Modal';
import { fonts } from '../utils/constants/fonts';
import { colors } from '../utils/constants/colors';
import { stringConstants } from '../utils/constants/stringConstants';
import BorderButton from './BorderButton';

const ConfirmationModal = (props) => {
    const { isShow, onClose, title, description, onCancel, onOk, btnText, btnStyle } = props;
    return (
        <CommonModal
            isShow={isShow ? isShow : false}
            onClose={onClose ? onClose : () => { }}
            closeIcon={true}>
            <View style={styles.viewContainerStyle}>
                <Text style={styles.titleStyle}>{title}</Text>
                <Text style={styles.descStyle}>{description}</Text>
                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-evenly", width: "100%",marginTop:55,marginHorizontal:25 }}>
                    {onCancel &&

                        <View style={[btnStyle, styles.buttonStyle, {marginRight:10}]}>
                            <BorderButton white={true}
                                title={stringConstants.cancelBtn}
                                onButtonPress={onCancel ? onCancel : () => { }} />
                        </View>
                        // <TouchableOpacity onPress={onCancel ? onCancel : () => { }}>
                        //     <Text style={{ color: colors.text_color, fontSize: 18, fontFamily: fonts.secondary_regular_font}}>{stringConstants.cancelBtn}</Text>
                        // </TouchableOpacity>
                    }
                    {onOk &&
                        <View style={[btnStyle, styles.buttonStyle]}>
                            <BorderButton
                                title={btnText ? btnText : stringConstants.okText}
                                onButtonPress={onOk ? onOk : () => { }} />
                        </View>
                    }
                </View>
            </View>
        </CommonModal>
    );
};

const styles = StyleSheet.create({
    viewContainerStyle: {
        alignItems: 'center',
        alignContent: 'center',
        // top: 15,
    },
    imageStyle: {
        marginBottom: 30
    },
    titleStyle: {
        marginTop: 40,
        fontSize: 22,
        textTransform: "capitalize",
        fontFamily: fonts.primary_bold_font,
        color: colors.text_color
    },
    descStyle: {
        textAlign: 'center',
        // marginBottom: 50,
        marginTop: 30,
        marginHorizontal:1,
        textTransform: "none",
        fontSize: 16,
        fontFamily: fonts.secondary_regular_font,
        color: colors.backgroundColor
    },
    buttonStyle: {
        width: 140,
        marginTop: -20
    },
    btnTxt: {
        width: '100%',
        marginHorizontal: 50,
        textTransform: "uppercase",
        textAlign: 'center'
    }
})

export default ConfirmationModal;