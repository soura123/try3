import React from 'react';
import Header from './header';
import { View, StyleSheet, ScrollView, KeyboardAvoidingView, } from 'react-native';
import { colors } from '../utils/constants/colors';
import { Content } from 'native-base';
import HeaderDashboard from './headerDashboard';

const CommonBackground = (props) => {
    const { headerTitle, onBackPress, skip, allowClick, sort, header, scroll, style, showVerticalIndicator, showHorizontalIndicator, backIcon, search, removeSkipText,removebackIcon } = props;
    return (
        <View style={styles.containerStyle}>
            {!header &&
                <Header
                    title={headerTitle ? headerTitle : null}
                    onBackPress={onBackPress ? onBackPress : () => { }}
                    skip={skip ? true : false}
                    removeSkip={removeSkipText ? true : false}
                    search={search ? true : false}
                    sort={sort ? true : false}
                    allowClick={allowClick ? allowClick : () => { }}
                    backIcon={backIcon ? true : false}
                    removebackIcon={removebackIcon ? true:false}
                />}
            {scroll ?
                <Content
                    contentContainerStyle={[styles.containerStyle, style]}
                    keyboardDismissMode='on-drag'
                    disableKBDismissScroll={false}
                    style={{ flex: 1 }}
                    keyboardShouldPersistTaps='handled'
                    showsHorizontalScrollIndicator={showHorizontalIndicator ? true : false}
                    showsVerticalScrollIndicator={showVerticalIndicator ? true : false}>{props.children}
                </Content>
                :
                <View style={[styles.containerStyle, style]}>{props.children}</View>
            }
        </View>
    );
};

const styles = StyleSheet.create({
    containerStyle: {
        flexGrow: 1,
        backgroundColor: '#E5E5E5',
        // d9d9d9
    },
})

export default CommonBackground;