// MULTI-ITEM PICKER:

import React, { Component } from 'react';
import {
    Modal,
    View,
    Text,
    SafeAreaView,
    StyleSheet,
    FlatList,
    TouchableOpacity,
    BackHandler,
} from 'react-native';
import { Icon } from 'native-base';
import { colors } from '../utils/constants/colors';
import stringConstants from '../utils/constants/stringConstants';
import fonts from '../utils/constants/fonts';

// console.log(colors, 'sdfsd')

class MultiItemPicker extends Component {
    // icon = checkedIcon;


    updateSelectedList = (value) => {
        const { props } = this;
        const selectedItems = [...props.selectedItems];
        const valueIndex = selectedItems.indexOf(value);
        if (props.isSingle) {
            valueIndex !== -1 ? props.onTap([]) : props.onTap([value]);
        } else {
            valueIndex !== -1
                ? selectedItems.splice(selectedItems.indexOf(value), 1)
                : selectedItems.push(value);
            props.onTap(selectedItems);
        }
    };

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.onClose();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    render() {
        const { props, updateSelectedList, icon } = this;
        return (
            <Modal visible={true} animationType={'slide'} requestOnClose={true}>
                <SafeAreaView style={styles.wrapper}>
                    <View style={styles.container}>
                        <View
                            style={[styles.header, {
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: "space-between",
                                padding: 5
                            }]
                            }
                        >
                            <Text style={[styles.headerText, {}]}
                            >
                                {props.title || 'Select Items'}
                            </Text>
                            {/* <Icon {...cross} onPress={props.onClose} /> */}
                            <TouchableOpacity onPress={props.onClose} style={styles.done}>
                                <Text style={{fontWeight:"bold"}}
                                >Done</Text>
                            </TouchableOpacity>
                        </View>
                        <FlatList
                            data={props.items}
                            showsVerticalScrollIndicator={false}
                            renderItem={({ item }) => {
                                return (
                                    <TouchableOpacity onPress={() => updateSelectedList(item.value)}>
                                        <View style={[styles.item, { justifyContent: "space-between", alignItems: "center", flexDirection: "row" }]}>
                                            <Text style={styles.text}>{item.label}</Text>
                                            {props.selectedItems.includes(item.value) && (
                                                <Icon style={styles.checked} {...icon} name='done' type='MaterialIcons' />
                                            )}
                                        </View>
                                    </TouchableOpacity>
                                );
                            }}
                            onEndReached={props.loadMore}
                            onEndReachedThreshold={0.1}
                        />
                    </View>
                </SafeAreaView>
            </Modal >
        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
    },

    container: {
        padding: 10,
    },
    checked: {
        color: colors.backgroundColor,
    },
    text: {
        fontSize: 17,
    },
    header: {
        backgroundColor: 'grey',
    },
    headerText: {
        fontSize: 18,
        textTransform: 'uppercase',
    },
    item: {
        paddingHorizontal: 15,
        borderBottomColor: colors.light_gray,
        borderBottomWidth: 1,
        minHeight: 55,
    },
    done: {
        padding: 10,
    },
});

export default MultiItemPicker;