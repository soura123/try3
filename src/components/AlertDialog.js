import { Alert } from 'react-native';


export function showDoubleActionAlertDialog(title, message, positiveTitle, positiveCallback, negativeTitle, negativeCallback) {
    Alert.alert(
        title,
        message,
        [
            {
                text: negativeTitle,
                style: 'cancel',
                onPress: () => negativeCallback()
            },
            {
                text: positiveTitle,
                style: 'destructive',
                onPress: () => positiveCallback(),
            }
        ],
        { cancelable: false },
    );
}