import React, { Component, useEffect } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { colors } from '../utils/constants/colors'
import { fonts } from '../utils/constants/fonts';
import { images } from '../utils/constants/assets';
import {elevationShadowStyle} from '../utils/globalFunction';
class BorderedButton extends Component {
	constructor(props) {
		super(props);

	}

	render() {
		let { title, btnStyle, isActive, onButtonPress, titleStyle, small } = this.props;
		/* title:-> this title is used to title of button */
		/* btnStyle:-> it is used to for custom style*/
		/* isActive:- it is used to have active state of button and on the bases of that button icons color is changed automatically */
		return (
			<TouchableOpacity
				onPress={onButtonPress ? onButtonPress : () => { }}
				disabled={this.props.disable}
				style={
					[this.props.white ? styles.buttonBorderActiveStyleWhite : styles.buttonBorderActiveStyle,
					this.props.shadowHeight ? {...elevationShadowStyle(this.props.shadowHeight)} : null,
					this.props.disable ? styles.disableButton : null,
						btnStyle, small ? { borderRadius: 30 } : null]
				}
			>
				<View style={[{ flex: 1, flexDirection: 'row', justifyContent: 'center',alignItems:'center' }]}>
					<Text
						numberOfLines={2}
						style={[styles.textActiveStyle, titleStyle ? {marginLeft:2} : {}, this.props.white ? styles.textActiveStyleWhite:{}]}
					>
						{title}
					</Text>
					{small ? <Image style={[{ height: 18, marginTop:2,marginLeft:7 }]} source={images.btnBack} /> : null
					}
				</View>
			</TouchableOpacity>
		);
	}
}
export default BorderedButton;

const styles = StyleSheet.create({
	buttonBorderUnactiveStyle: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
		paddingVertical: 10,
		borderColor: "#72DADB",
		borderWidth: 2,
		marginHorizontal: 5,
	},
	buttonBorderActiveStyle: {
		paddingVertical: 12,
		// borderColor: colors.theme_primary_color,
		minHeight: 50,
		justifyContent: 'center',
		backgroundColor: colors.primary_action_button_background_theme,
		borderRadius: 6,
		width: "100%",
		alignSelf: 'center',
		marginTop: 20,
		// marginHorizontal: 5,
	},
	buttonBorderActiveStyleWhite: {
		paddingVertical: 12,
		justifyContent: 'center',
		borderColor: colors.theme_primary_color,
		minHeight: 50,
		borderWidth: 1, alignSelf: 'center',
		backgroundColor: "white",
		width: "100%",
		marginTop: 20,
		borderRadius:6
	},
	imageStyle: {
		width: 35,
		resizeMode: 'contain',
		alignSelf: 'center',
		marginLeft: 5,
	},
	textActiveStyleWhite: {
		color: colors.backgroundColor,
		// width: '50%',
		fontSize: 17,
		marginHorizontal: 5,
		fontFamily: fonts.primary_semi_bold_font,
		alignSelf: 'center',
	},
	textActiveStyle: {
		color: colors.primary_action_button_text_theme,
		fontSize: 17,
		marginHorizontal: 5,
		fontFamily: fonts.primary_semi_bold_font,
		alignSelf: 'center',
	},
	textUctiveStyle: {
		color: 'red',
		fontSize: 11,
		marginHorizontal: 5,
		alignSelf: 'center',
		textTransform: 'uppercase'
	},
	shadowButton: {
		
		
		// elevation:5,
		// paddingVertical: 15,
		// shadowColor: "rgba(185, 146, 85, 0.5)",
		// marginVertical: 5
	},
	disableButton: {
		backgroundColor: "grey",
		color: "grey"

	}
});
