import React from 'react';
import { ActivityIndicator, StyleSheet, View, Dimensions } from 'react-native';
import { connect } from 'react-redux';

const { width, height } = Dimensions.get('window');


const Loader = (props) => {
    return (
        <React.Fragment>
            {props.loading
                ?
                <View style={[styles.container, styles.horizontal]}>
                    <ActivityIndicator size="large" color="#000" />
                </View>
                :
                null
            }
        </React.Fragment>
    );
};

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        backgroundColor: '#ffffff',
        position: 'absolute',
        height: height,
        width: width,
        zIndex: 9999,
        opacity: 0.3,
    },
    wrapper: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 10,
    },
});

const mapStateToProps = (state) => {
    return {
        loading: state.UiLoader.globalLoader,
    };
};

export default connect(mapStateToProps)(Loader);
