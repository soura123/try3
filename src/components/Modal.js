import React from 'react';
import { Modal, TouchableOpacity, TouchableWithoutFeedback, StyleSheet, View, Image } from 'react-native';
import { colors } from '../utils/constants/colors';
import { images } from '../utils/constants/assets';

const CommonModal = (props) => {
    const { isShow, onClose, customBodyStyle, closeIcon, customIconStyle } = props;
    return (
        <View style={[{}]}>
        <Modal
            visible={isShow ? isShow : false}
            onRequestClose={onClose ? onClose : () => { }}
            transparent={true}
            animationType={'slide'}>
                
            <View
                activeOpacity={1}
                style={styles.overlayStyle}>
                <TouchableWithoutFeedback>
                    <View style={[styles.mainBodyStyle]}>
                        {!closeIcon ?
                            <TouchableOpacity
                                onPress={onClose ? onClose : () => { }}
                                style={[styles.closeIcon, customIconStyle]}>
                                <Image source={images.closeIcon} resizeMode='cover' />
                            </TouchableOpacity> : null}
                        <View style={[styles.subBodyStyle, customBodyStyle]}>
                            {props.children}
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        </Modal>
        </View>
    );
};

const styles = StyleSheet.create({
    overlayStyle: {
        
        flex: 1,
        marginTop:245,
        // justifyContent: 'center',
        // alignItems: 'center',
        backgroundColor: 'white',
        maxHeight:284,
        alignSelf:'center',
        marginHorizontal:25,
        borderRadius:10
    },
    mainBodyStyle: {
        margin: 25,
        marginVertical: 40,
        justifyContent: 'center',
        alignItems: 'center',
        width: 310,
        height: '60%'
    },
    subBodyStyle: {
        backgroundColor: colors.white,
        borderRadius: 20,
        padding: 25,
        maxHeight: 600,
        minHeight: 300,
    },
    closeIcon: {
        alignSelf: 'flex-end',
    }
});

export default CommonModal;