import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { images } from '../utils/constants/assets';
import { globalStyles } from '../utils/globalStyles'
import { colors } from '../utils/constants/colors';
import { fonts } from '../utils/constants/fonts';

export default class HeaderDashboard extends Component {
	render() {
		const { title, onBackPress, skip, allowClick, backIcon } = this.props;
		return (
			<View style={[styles.container,]}>
				{backIcon ?
					<TouchableOpacity
						style={{ height: 50, width: 50, overflow: "hidden", marginLeft: 10 }}
						activeOpacity={.5}
						onPress={onBackPress ? onBackPress : () => { }}>
						<Image source={images.search} resizeMode="contain" style={{ width: '100%', height: '100%' }} />
					</TouchableOpacity>
					: null}
				<Text
					numberOfLines={1}
					style={[globalStyles.title, styles.title]}
				>{title}
				</Text>
				{skip ?
					<Text
						style={[styles.skip]}
						onPress={allowClick ? allowClick : () => { }}>
						Skip</Text>
					: <Text
						style={[styles.skip]}
						onPress={allowClick ? allowClick : () => { }}>
					</Text>}
			</View>
		);
	}
}

const styles = StyleSheet.create({

	title: {
		alignSelf: 'center',
		backgroundColor: 'transparent',
	},
	skip: {
		fontSize: 14,
		fontFamily: fonts.secondary_semi_bold_font,
		alignSelf: 'center',
		marginTop: 4,
		color: colors.backgroundColor,
		marginRight: 25

	},
	container: {
		marginTop: 10,
		flexDirection: 'row',
		height: 50,
		justifyContent: 'space-between',
		width: '100%',
		backgroundColor: 'transparent',
		// zIndex: 200,
	}
});
