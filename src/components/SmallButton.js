import React, { Component, useEffect } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { colors } from '../utils/constants/colors'
import { fonts } from '../utils/constants/fonts';
import { images } from '../utils/constants/assets';
class SmallButton extends Component {
	constructor(props) {
		super(props);

	}

	render() {
		let { title, btnStyle, isActive, onButtonPress, titleStyle } = this.props;
		/* title:-> this title is used to title of button */
		/* btnStyle:-> it is used to for custom style*/
		/* isActive:- it is used to have active state of button and on the bases of that button icons color is changed automatically */
		return (
			<TouchableOpacity
				onPress={onButtonPress ? onButtonPress : () => { }}
				disabled={this.props.disable}
				style={
					[this.props.white ? styles.buttonBorderActiveStyleWhite : styles.buttonBorderActiveStyle,
					this.props.shadow ? styles.shadowButton : null,
					this.props.disable ? styles.disableButton : null,
						btnStyle, { borderRadius: 30 }]
				}
			>
				<View style={[{ flex: 1, flexDirection: 'row', justifyContent: 'center' }]}>
					{this.props.icon == "facebook" && <Image source={images.facebook} style={styles.imageStyle} />}
					{this.props.icon == "google" && <Image source={images.google} style={styles.imageStyle} />}
					<Text
						numberOfLines={2}
						style={[styles.textActiveStyle, titleStyle ? titleStyle : {}]}
					>
						{title}

					</Text>
					<Image style={[{ paddingHorizontal: 8, marginTop: 5, width: 1, height: 20 }]} source={images.btnBack} />
				</View>
			</TouchableOpacity>
		);
	}
}
export default SmallButton;

const styles = StyleSheet.create({
	buttonBorderUnactiveStyle: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
		paddingVertical: 10,
		borderColor: "#72DADB",
		borderWidth: 2,
		marginHorizontal: 5,
	},
	buttonBorderActiveStyle: {
		paddingVertical: 12,
		// borderColor: colors.theme_primary_color,
		minHeight: 50,
		justifyContent: 'center',
		backgroundColor: colors.primary_action_button_background_theme,
		borderRadius: 6,
		width: "100%",
		alignSelf: 'center',
		marginTop: 20,
		// marginHorizontal: 5,
	},
	buttonBorderActiveStyleWhite: {
		paddingVertical: 12,
		justifyContent: 'center',
		borderColor: colors.theme_primary_color,
		minHeight: 50,
		borderWidth: 2, alignSelf: 'center',
		backgroundColor: "white",
		width: "100%",
		marginTop: 20
	},
	imageStyle: {
		width: 35,
		resizeMode: 'contain',
		alignSelf: 'center',
		marginLeft: 5,
	},
	textActiveStyleWhite: {
		color: "white",
		// width: '50%',
		fontSize: 17,
		marginHorizontal: 5,
		fontFamily: fonts.primary_semi_bold_font,
		alignSelf: 'center',
	},
	textActiveStyle: {
		color: colors.primary_action_button_text_theme,
		fontSize: 17,
		marginHorizontal: 5,
		fontFamily: fonts.primary_semi_bold_font,
		alignSelf: 'center',
	},
	textUctiveStyle: {
		color: 'red',
		fontSize: 11,
		marginHorizontal: 5,
		alignSelf: 'center',
		textTransform: 'uppercase'
	},
	shadowButton: {
		shadowColor: "black",
		elevation: 10,
		paddingVertical: 15,
		marginVertical: 5
	},
	disableButton: {
		backgroundColor: "grey",
		color: "grey"

	}
});
