import { actionConstants } from "../utils/constants/actionConstants"
import { appConstants } from "../utils/constants/appConstants"

export const actionCreators = {
    showLoader: {
        [actionConstants.action_type]: actionConstants.show_loader,
        globalLoader: true,
    },
    hideLoader: {
        [actionConstants.action_type]: actionConstants.show_loader,
        globalLoader: false,
    },
    errorHandler: function (errorMessage: string) {
        return {
            [actionConstants.action_type]: actionConstants.error_handler,
            [appConstants.error_message]: errorMessage,
        }
    },
    fetchUserDetail: function (payload: string) {
        return {
            type: actionConstants.login_success,
            [appConstants.user_data]: payload,
        }
    },
    logout_success: function (payload: string) {
        return {
            type: actionConstants.logout_success,
            [appConstants.user_data]: payload,
        }
    },
    exceptionHandler: function (exception: any) {
        return {
            [actionConstants.action_type]: actionConstants.error_handler,
            [appConstants.exception_object]: exception,
        }
    },
}