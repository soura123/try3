import axios from 'axios'
import { endPoints } from '../utils/constants/endPoints';
import { apiConstants } from '../utils/constants/apiConstants';
import { objToFormData, logOnConsole, isNotEmpty } from '../utils/globalFunction';
import AsyncStorage from '@react-native-community/async-storage';

const TIMEOUT_DURATION_IN_MILLIS = 60000;

const defaultInstance = axios.create({
    baseURL: endPoints.base_url,
    timeout: TIMEOUT_DURATION_IN_MILLIS,
    validateStatus: function (status) {
        return status >= 200 && status < 500;
    },
});

defaultInstance.interceptors.request.use(
   async (config: any) => {
        logOnConsole("API Request Config:", config);
        if (config.data) {
            if (config.data instanceof FormData) {
                config.headers[apiConstants.content_type_key] = apiConstants.form_data_type;
            } else {
                config.headers[apiConstants.content_type_key] = apiConstants.raw_data_type;
            }
        }

        let token = '';
        let authData = await AsyncStorage.getItem(apiConstants.user_data_key);
        if (isNotEmpty(authData)) {
            token = JSON.parse(authData).api_key
        }
        if (token) {
            config.headers.Authorization = 'Token token=' + token;;
            // 'Token token=' + token;
        }
        
        logOnConsole("API Request Config:", config);
        return config;
    },
    (error: any) => {
        // Do something with request error
        logOnConsole("Request Error:", error);
        return Promise.reject(error);
    });

defaultInstance.interceptors.response.use(
    (response: any) => {
        return response.data
    },
    (error: any) => {
        // Do something with request error
        logOnConsole("Response Error:", error);
        return Promise.reject(error);
    });

const getRequestData = (data: any, contentType: String) => {
    switch (contentType) {
        case apiConstants.raw_data_type:
            return data;
        case apiConstants.multipart_data_type:
        case apiConstants.form_data_type:
            return objToFormData(data);
    }
    return data;
}

export const prepareApiRequest = (url: string, apiRequestType: String, contentType: String, params: any, body: any, successCallback: Function, errorCallback: Function, exceptionCallback: Function) => {
    if (isNotEmpty(params))
        body = params
    requestApi(url, apiRequestType, getRequestData(body, contentType), successCallback, errorCallback, exceptionCallback);
}

const requestApi = (url: string, apiRequestType: String, data: any, successCallback: Function, errorCallback: Function, exceptionCallback: Function) => {
    let promise: Promise<any> = null;
    switch (apiRequestType) {
        case apiConstants.get_request_type:
            logOnConsole("GET Request")
            promise = defaultInstance.get(url, { params: data });
            break;
        case apiConstants.post_request_type:
            logOnConsole("POST Request")
            promise = defaultInstance.post(url, data);
            break;
        case apiConstants.patch_request_type:
            logOnConsole("PATCH Request")
            promise = defaultInstance.patch(url, data);
            break;
        case apiConstants.put_request_type:
            logOnConsole("PUT Request")
            promise = defaultInstance.put(url, data);
            break;
        case apiConstants.delete_request_type:
            logOnConsole("DELETE Request")
            promise = defaultInstance.delete(url, { data: data });
            break;
    }

    promise.then((response: any) => {
        // Success Condition
        if (true) {
            if (successCallback) {
                successCallback(response);
            }
        } else {
            // Error Condition
            if (errorCallback) {
                errorCallback();
            }
        }
    }).catch((ex: any) => {
        // Handle Exception
        if (exceptionCallback)
            exceptionCallback(ex)
    })
}