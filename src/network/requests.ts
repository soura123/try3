import { shouldShowTopLoaderOnListing, isDataPaginating, isNotEmpty } from "../utils/globalFunction"
import { prepareApiRequest } from './index'
import { apiConstants } from "../utils/constants/apiConstants";
import { endPoints } from "../utils/constants/endPoints";
import { actionCreators } from "../actions/actionCreators";

var loaderCount = 0;
export const doSignIn = (data: any, succesCallback: Function, errorCallback: Function) => {
    return commonApiWrapper(endPoints.sign_in
        , apiConstants.post_request_type
        , apiConstants.raw_data_type
        , null, data, null,
        (response: any, dispatch: any) => {
            dispatch(actionCreators.fetchUserDetail(response))
            if (succesCallback){
                // showLoader(false, dispatch);
                succesCallback(response)
            }
        },
        
        // succesCallback,
         errorCallback)
}

export const doSignUp = (data: any, succesCallback: Function, errorCallback: Function) => {
    return commonApiWrapper(endPoints.sign_up
        , apiConstants.post_request_type
        , apiConstants.raw_data_type
        , null, data, null,
         (response: any, dispatch: any) => {
            dispatch(actionCreators.fetchUserDetail(response))
            if (succesCallback){
                // showLoader(false,dispatch);
                succesCallback(response)}
        },
        // succesCallback,
         errorCallback)
}

export const doResetPassword = (data: any, succesCallback: Function, errorCallback: Function) => {
    return commonApiWrapper(endPoints.reset_password
        , apiConstants.post_request_type
        , apiConstants.raw_data_type
        , null, data, null, succesCallback, errorCallback)
}

export const sendOtp = (succesCallback: Function, errorCallback: Function) => {
    return commonApiWrapper(endPoints.send_otp
        , apiConstants.post_request_type
        , apiConstants.raw_data_type
        , null, null, null, (response: any, dispatch: any) => {
            if (succesCallback)
                succesCallback(response)
        }, errorCallback)
}

export const changePassword = (data: any,succesCallback: Function, errorCallback: Function) => {
    return commonApiWrapper(endPoints.change_password
        , apiConstants.put_request_type
        , apiConstants.raw_data_type
        , null, data, null, (response: any, dispatch: any) => {
            // dispatch(actionCreators.updateProfile(response))
            if (succesCallback){
                console.log("in requsestdss",response);
                succesCallback(response)
            }
        }, errorCallback)
}


export const verifyOtp = (data: any, succesCallback: Function, errorCallback: Function) => {
    return commonApiWrapper(endPoints.verify_otp
        , apiConstants.post_request_type
        , apiConstants.raw_data_type
        , null, data, null, (response: any, dispatch: any) => {
            // dispatch(actionCreators.updateProfile(response))
            if (succesCallback)
                succesCallback(response)
        }, errorCallback)
}


export const doSignOut = (succesCallback: Function, errorCallback: Function) => {
    return commonApiWrapper(endPoints.sign_out
        , apiConstants.delete_request_type
        , null
        , null, null, null, (response: any, dispatch, any) => {

            dispatch(actionCreators.logout_success(''))
            if (succesCallback)
                succesCallback(response)
        }, errorCallback)
}

const flatlistWrapper = (url: string, typeOfRequest: String, nextPageUrl: string, succesCallback: Function, errorCallback: Function, params: any, path: string) => {
    return (dispatch: any) => {
        if (shouldShowTopLoaderOnListing(typeOfRequest)) {
            showLoader(true, dispatch);
        }
        if (isDataPaginating(typeOfRequest) && isNotEmpty(nextPageUrl)) {
            url = nextPageUrl;
        }

        prepareApiRequest(url, apiConstants.get_request_type, apiConstants.raw_data_type, params, null, (response: any) => {
            succesCallback(response[apiConstants.data_key], response[apiConstants.next_page_url_key])
            if (shouldShowTopLoaderOnListing(typeOfRequest)) {
                showLoader(false, dispatch);
            }
        },
            (errorMessage: string) => handleError(errorMessage, errorCallback, dispatch),
            (exception: any) => handleException(exception, dispatch),
        )
    }
}

const commonApiWrapper = (url: string, apiRequestType: String, contentType: String, path: string, requestData: any, params: any, successCallback: Function, errorCallback: Function) => {
    return (dispatch: any) => {
        showLoader(true, dispatch);
        if (isNotEmpty(path))
            url = `${url}${path}/`
        prepareApiRequest(url, apiRequestType, contentType, params, requestData, 
            (response:any)=> {showLoader(false,dispatch); successCallback(response,dispatch)},
            (errorMessage: string) => handleError(errorMessage, errorCallback, dispatch),
            (exception: any) => handleException(exception, dispatch),
        )
    }
}

// const commonApiWrapper = (url: string, apiRequestType: String, contentType: String, path: string, requestData: any, params: any, successCallback: Function, errorCallback: Function) => {
//     return (dispatch: any) => {
//         loaderCount++;
//         showLoader(true, dispatch);
//         if (isNotEmpty(path))
//             url = `${url}${path}/`
//         prepareApiRequest(url, apiRequestType, contentType, params, requestData, (response: any) => {
//             loaderCount--;
//             if (loaderCount == 0)
//                 showLoader(false, dispatch);
//             successCallback(response, dispatch)
//         },
//             (errorMessage: any, status: number) => handleError(errorMessage, status, errorCallback, dispatch),
//             (exception: any) => handleException(exception, dispatch),
//         )
//     }
// }

const showLoader = (shouldShow: boolean, dispatch: any) => {
    if (shouldShow) {
        dispatch(actionCreators.showLoader)
    } else {
        dispatch(actionCreators.hideLoader)
    }
}

const handleError = (errorMessage: string, errorCallback: Function, dispatch: any) => {
    showLoader(false, dispatch);
    if (isNotEmpty(errorMessage))
        dispatch(actionCreators.errorHandler(errorMessage));
    if (errorCallback)
        errorCallback()
}





const handleException = (exception: any, dispatch: any) => {
    showLoader(false, dispatch);
    if (isNotEmpty(exception))
        dispatch(actionCreators.exceptionHandler(exception));
}