import { Toast } from 'native-base';

const ShowToast = (type, message, onClose) => {
    return Toast.show({
        text: message,
        duration: 4000,
        type: type,
        position: 'bottom',
        style: {
            borderRadius: 3,
            margin: 5,
            marginTop: 40,
        },
        // onClose: onClose,
    });
};

export default ShowToast;