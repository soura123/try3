export const actionConstants = {

    user_detail_fetched: 'user_detail_fetched',
    login_success: 'login_success',
    sign_up_success: 'sign_up_success',

    log_out_success: 'log_out_success',
    log_out_failure: 'log_out_failure',

    show_loader: 'show_loader',
    error_handler: 'error_handler',
    exception_handler: 'exception_handler',
    action_type: 'type',
    logout_success:'logout_success',


}