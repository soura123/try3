const base_theme_color = '#C2C2C2'
const secondary_base_theme_color = '#FFFFFF'

export const colors = {
    // Theme Colors used for App
    primary_theme: '#202020',
    secondary_theme: base_theme_color,

    // Background Color
    backgroundColor:'#202020',
    backgroundSecondColor:'#E5E5E5',
    primary_background_color: secondary_base_theme_color,
    secondary_background_color: base_theme_color,

    // Text Title Colors
    title_text_theme: '#000000',
    subtitle_text_theme: '#EDEDED',
    dotColor:'#BDBDBD',

    // Clickable Text Color
    primary_action_text_theme: base_theme_color,
    secondary_action_text_theme: base_theme_color,

    // Button Text Color
    
    primary_action_button_text_theme: '#FFFFFF',
    secondary_action_button_text_theme: base_theme_color,

    // Button Background Color
    primary_action_button_background_theme: '#B99255',
    secondary_action_button_background_theme: secondary_base_theme_color,

    // Button Border Color
    primary_action_button_stroke_theme: 'transparent',
    secondary_action_button_stroke_theme: base_theme_color,

    border_color:'#F2F2F2',

    // Translucent
    modal_background: 'rgba(0,0,0,0.8)'

}