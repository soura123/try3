// const getImagePath = (imageName: String) => {
//     return `../../assets/images/${imageName}`;
// }

export const images = {

    // example:require('../../assets/images/example.png'),
    logo: require('../../assets/images/logo.png'),
    background: require('../../assets/images/background.png'),
    onboard: require('../../assets/images/onboard.png'),
    backBtn: require('../../assets/images/backBtn.png'),
    btnBack: require('../../assets/images/btnBack.png'),
    sortImg: require('../../assets/images/sortImg.png'),
    search: require('../../assets/images/search.png'),
    tab1: require('../../assets/images/tabOne.png'),
    tab1Active: require('../../assets/images/tabOneActive.png'),
    tab2: require('../../assets/images/tabTwo.png'),
    tab2Active: require('../../assets/images/tabTwoActive.png'),
    tab3: require('../../assets/images/tabThree.png'),
    tab3Active: require('../../assets/images/tabThreeActive.png'),
    tab4: require('../../assets/images/tabFour.png'),
    tab4Active: require('../../assets/images/tabFourActive.png'),
    settingBackIcon: require('../../assets/images/settingBackIcon.png'),
    closeIcon: require('../../assets/images/closeIcon.png'),
    bell     : require('../../assets/images/bell.png'),
    user     :require('../../assets/images/user1.png'),
    arrow: require('../../assets/images/Arrow.png')



}