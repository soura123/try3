export const apiConstants = {
    content_type_key: 'Content-Type',
    authorization_key: 'Authorization',
    form_data_type: 'multipart/form-data',
    raw_data_type: 'application/json',
    multipart_data_type: 'multipart/form-data',
    get_request_type:'GET',
    patch_request_type:'PATCH',
    post_request_type:'POST',
    put_request_type:'PUT',
    delete_request_type:'DELETE',


    data_key: 'data',
    access_token_key: 'access_token',
    refresh_token_key: 'refresh_token',
    // user_data_key: 'data',
    user_data_key: 'user_data',
    api_token_key:'api_key',

    next_page_url_key: 'next_page_url',
}