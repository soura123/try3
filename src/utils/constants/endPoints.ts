import { appConstants } from "./appConstants"

const current_flavour = appConstants.flavour_dev;

const getBaseConfig = (flavour: String) => {
    let obj = { base_url: '' }
    switch (flavour) {
        case appConstants.flavour_dev:
            obj.base_url = 'https://demo-law-link.herokuapp.com/api/v1/';
            break;
        case appConstants.flavour_staging:
            obj.base_url = 'https://demo-law-link.herokuapp.com/api/v1/';
            break;
        case appConstants.flavour_production:
            obj.base_url = 'https://demo-law-link.herokuapp.com/api/v1/';
            break;
    }
    return obj;
}



export const endPoints = {

    ...getBaseConfig(current_flavour),
    sign_in: 'authorizations/sign_in.json',
    sign_up:'authorizations.json',
    reset_password: 'authorizations/password.json',
    send_otp      :'mobile_verification/send_otp.json',
    verify_otp    :'mobile_verification/verify_otp.json',
    sign_out      :'authorizations/sign_out.json',
    example: 'example/',
    change_password:'users/update_password.json'

}