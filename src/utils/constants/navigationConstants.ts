export const navigationConstants = {

     // Stacks
     settings_stack: 'settings_stack',
     chat_stack: 'chat_stack',
     redirect_auth: 'redirect_auth',
 
 
     // Drawer
 
 
     // Switch
     unauthorized_user_switch: 'unauthorized_user_switch',
 
     // Tab Bar

     TAB_BAR: 'TAB_BAR',
	 TAB_BAR_WRAPPER: 'TAB_BAR_WRAPPER',
	 DASHBOARD_TAB: 'DASHBOARD_TAB',
	 OFFERS_TAB: 'OFFERS_TAB',
	 REWARDS_TAB: 'REWARDS_TAB',
	 PROFILE_TAB: 'PROFILE_TAB',
 
 
     // Individual
     onboarding_screen: 'onboarding_screen',
    legal_info_screen: 'legal_info_screen',
    dashboard_screen: 'dashboard_screen',
    login_screen: "login_screen",
    signup_screen: "signup_screen",
    signup_step_second_screen: "signup_step_second_screen",
    signup_phone_number: "signup_phone_number",
    signup_mobile_number_verification: "signup_mobile_number_verification",
    forgot_password: 'forgot_password',
    change_password: 'change_password',
    account_settings: 'account_settings',
    notification: 'notification',
    dummy: 'dummy',
    tab_bar: 'tab_bar',
    tab_bar_wrapper: 'tab_bar_wrapper',
    booking_screen: 'booking_screen',
    location_screen: 'location_screen',
    allow_notification_screen: 'allow_notification_screen',
    about_app_screen: 'about_app_screen',
    legal_screen: 'legal_screen',
    create_spot_screen: "create_spot_screen",
    profile_detail_screen: "profile_detail_screen",
    add_bank_detail : 'add_bank_detail',

    // Drawer
    
    
    // Switch
    
    
    // Tab Bar
    
    
    // Individual



}
