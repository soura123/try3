export const appConstants = {

    flavour_dev: 'DEV',
    flavour_staging: 'STAGING',
    flavour_production: 'PRODUCTION',

    device_android: 'android',
    device_ios: 'ios',

    data: 'data',
    user_data: 'user_data',

    enable_loader: 'enable_loader',
    error_message: 'error_message',
    exception_object: 'exception_object',


    // Custom FlatList
    flatlist_first_time_request: 'flatlist_first_time_request',
    flatlist_refreshing_request: 'flatlist_refreshing_data',
    flatlist_pagination_request: 'flatlist_pagination_request',


}