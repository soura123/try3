import { name as appName } from '../../../app.json';
import { displayVersion as version } from '../../../app.json';


export const stringConstants = {

    app_name: appName,
    app_version: version,
    ok_text: 'ok',
    get_started: 'GET STARTED',
    get_started_screen_text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    login_text: 'Login',
    skip_text: 'Skip',
    email_address_text: 'Email Address',
    FIRST_NAME_KEY: 'firstname',
    LAST_NAME_KEY: 'lastname',
    EMAIL_KEY: 'email',
    MOBILE_NUMBER_KEY: 'mobile_number',
    PASSWORD_KEY: 'password',
    EMAIL_TITLE: 'Email',
    PASSWORD_TITLE: 'Password',
    DONE_TEXT: 'done',
    NEXT_TEXT: 'next',
    CREATE_PASSWORD_TEXT: 'Create Password',
    BY_CLICK_BELOW_AGREE_TEXT: 'By clicking below, I agree to the',
    TERMS_OF_USE_TEXT: ' Terms of Use ',
    AND_READ_THE_TEXT: 'and have read the ',
    PRIVACY_STATEMENT_TEXT: 'Privacy Statement.',
    FORGOT_PASSWORD_TEXT: 'Forgot Password?',
    LOGIN_BTN: 'LOGIN',
    SIGNUP_TEXT: '  Sign up',
    CREATE_AN_ACCOUNT_TEXT: 'Create an account?',
    CREATE_ACCOUNT_HEADING: 'Create an account',
    FIRST_NAME_TEXT: 'First Name',
    LAST_NAME: 'Last Name',
    MOBILE_NUMBER: 'Mobile Number',
    FIRST_NAME_TITLE: 'Firstname',
    LAST_NAME_TITLE: 'Lastname',
    MOBILE_NUMBER_TITLE: 'Mobile Number',
    SIGN_UP_BTN: 'SIGN UP',
    LOGIN_TEXT: ' Login',
    ALREADY_ACCOUNT_TEXT: 'Already have an account? ',
    FORGOT_PASSWORD_HEADING: 'Forgot your Password?',
    ENTER_YOUR_EMAIL_BELOW_TEXT: 'Enter your email below to receive instruction on how to reset your password.',
    SEND_BTN: 'SEND',
    RESEND_CODE_TEXT: 'Resend Code',
    NEXT_BTN: 'Next',
    MOBILE_VERIFICATION_HEADING: "Mobile Verification",
    toast_success: 'success',
    toast_danger: 'danger',
    signup_success: 'Sign up successfully',
    sigin_success: 'Sign in successfully',
    FORGOT_SUCCESS: 'Password reset link has been sent to your email',
    otp_verify: 'OTP verify successfully',
    okText: 'Ok',
    cancelBtn: 'Cancel',
    sign_out_text: 'Sign Out',
    log_out_heading: 'Sign Out',
    logout_description_text: 'Are you sure you want to signout?',
    signin_required: 'Sign in required',
    signin_required_desc: 'Please Login to continue',
    login_btn_text: 'Login',
    old_password:'Old Password',
    confirm_password:'Confirm Password',
    new_password:'New Password',
    change_password_btn:'Change Password',
    change_password_success: 'Password updated successfully',
    rating_review: 'Ratings & Review',
    payment_method: 'Payment Method',
    change_password: 'Change Password',
    onboarding_tutorial:'Onboarding Tutorial',
    term_use           : 'Terms of Use',
    privacy_policy     :'Privacy Policy',
    sign_out           :'Sign Out',
    delete_account     :'Delete My Account',
    profile            :'Profile',
    












}