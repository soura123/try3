import { isAndroid } from "../globalFunction"

const primary_font_android = 'SF-Pro-Text-'
const primary_font_ios = 'SFProText-'
const secondary_font_android = 'SF-Pro-Display-'
const secondary_font_ios = 'SFProDisplay-'

export const fonts = {
    primary_regular_font: isAndroid() ? `${primary_font_android}Regular` : `${primary_font_ios}Regular`,
    primary_semi_bold_font: isAndroid() ? `${primary_font_android}Semibold` : `${primary_font_ios}Semibold`,
    primary_bold_font: isAndroid() ? `${primary_font_android}Bold` : `${primary_font_ios}Bold`,
    primary_medium_font: isAndroid() ? `${primary_font_android}Medium` : `${primary_font_ios}Medium`,

    secondary_regular_font: isAndroid() ? `${secondary_font_android}Regular` : `${secondary_font_ios}Regular`,
    secondary_semi_bold_font: isAndroid() ? `${secondary_font_android}Semibold` : `${secondary_font_ios}Semibold`,
    secondary_bold_font: isAndroid() ? `${secondary_font_android}Bold` : `${secondary_font_ios}Bold`,
    secondary_medium_font: isAndroid() ? `${secondary_font_android}Medium` : `${secondary_font_ios}Medium`,

}

export const fontSize = {
    title_size: 20,
    subtitle_size: 18,
    header_title_size: 28,
}

