import { Platform } from 'react-native';
import { appConstants } from './constants/appConstants';
import { colors } from './constants/colors'

export const isAndroid = () => {
	return Platform.OS === appConstants.device_android;
}

export const isIos = () => {
	return Platform.OS === appConstants.device_ios;
}

export const elevationShadowStyle = (elevation: any) => {
	return {
	  elevation,
	  shadowColor: 'rgba(185, 146, 85, 0.5)',
	  shadowOffset: { width: 0, height: 0.5 * elevation },
	  shadowOpacity: 0.3,
	  shadowRadius: 4 * elevation
	};
  }
  
export const emptyFunction = () => { }

export const createReducer = (initialState: any, handlers: any) => {
	return function reducer(state = initialState, action: any) {
		if (handlers.hasOwnProperty(action.type)) {
			return handlers[action.type](state, action);
		} else {
			return state;
		}
	};
}

export const objToFormData = (rawData: any) => {
	let formData: FormData = new FormData();
	if (rawData && rawData != null && typeof (rawData) === 'object') {
		Object.keys(rawData).map((item, index) => {
			formData.append(item, rawData[item]);
		})
	}
	return formData;
}

export const isNotEmpty = (data: any) => {
	return data !== null && data !== undefined && data !== '';
}

export const logOnConsole = (...arg: any) => {
	if (__DEV__)
		console.log(arg);
}


export const isDataRefreshing = (typeOfRequest: String) => {
	return typeOfRequest === appConstants.flatlist_refreshing_request;
}

export const isDataPaginating = (typeOfRequest: String) => {
	return typeOfRequest === appConstants.flatlist_pagination_request;
}

export const shouldShowTopLoaderOnListing = (typeOfRequest: String) => {
	return typeOfRequest === appConstants.flatlist_first_time_request;
}


export const goToBack = (key, reference, context) => {
	return (key === "Backspace" && context[reference]) ? context[reference].current.focus() : null;
}