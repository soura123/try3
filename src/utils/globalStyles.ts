import { StyleSheet } from 'react-native';
import { colors } from './constants/colors';
import { fontSize, fonts } from './constants/fonts';

export const globalStyles = StyleSheet.create({
    flex1: {
        flex: 1,
    },
    flex1WithBackground: {
        flex: 1,
        backgroundColor: colors.primary_background_color
    },
    standardMarginVertical: {
        marginVertical: 10,
    },
    standardMarginHorizontal: {
        marginHorizontal: 20,
    },
    title: {
        fontSize: fontSize.title_size,
        fontFamily: fonts.secondary_bold_font,
        color: colors.primary_theme,
    },
    actionTitle: {
        fontSize: fontSize.title_size,
        fontFamily: fonts.primary_semi_bold_font,
        color: colors.primary_action_text_theme,
    },
    subtitle: {
        fontSize: fontSize.title_size,
        fontFamily: fonts.primary_semi_bold_font,
        color: colors.subtitle_text_theme,
    },
    textInputHeading:{ 
        marginBottom: 10,
        fontFamily: fonts.primary_medium_font,
        fontSize: 14,
        color: colors.backgroundColor },
    
})