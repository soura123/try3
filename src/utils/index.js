import { Platform } from 'react-native';
import showToast from './showToast';
// import TextString from './constants/Strings/TextString';


const goToNextInput = (reference, context) => {
    console.log(reference, context)
    return context[reference] ? context[reference].current.focus() : null;
};

// Platform checks
const isAndroid = Platform.OS === 'android';
const isIos = Platform.OS === 'ios';

// Conver First letter of string to uppercase
const firstUC = (string) =>
    `${string.substring(0, 1).toUpperCase()}${string.substring(1)}`;

// Error Toast
const showErrorMessage = (message) => {
    showToast('danger', message);
};

const isNotEmpty = (item) => {
    return item !== undefined && item !== false && item != 0 && item !== null && item != '';
};

// Form Validations
const validateEmail = (value) => {
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    return emailPattern.test(value);
};

//password validation
const validatePassword = (value) => {
    var passwordPattern = /^.{8,}$/;
    return passwordPattern.test(value);
    // var passwordPattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,}$/;    
    // should contain at least one lower case  /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/;
    // should contain at least one digit
    // should contain at least one upper case
    // should contain at least 8 from the mentioned characters

};
// ^(((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,20})
// generate form data
const generateFormData = (item) => {
    const data = {};
    for (let key in item) {
        const value = typeof item[key].value === 'string' ? item[key].value.trim() : item[key].value;
        if (key !== "tncAndPPKey") {
            data[key] = value;
        }
    }
    return data;
};

const validateForm = (formData, options = {}) => {
    const email_keys = []; //all keys with email validations required
    const password_keys = [];
    let isFormValid = true;
    let isRequiredFieldsValid = false;

    // TODO 1: REQUIRED FIELDS
    Object.keys(formData).every((key, index) => {
        const { validations, value, title } = formData[key];

        if (validations && validations.isRequired && typeof value === 'string' &&
            value.trim() === '') {
            const message = `${title ? firstUC(title) : firstUC(key)} is required.`;

            if (!options.modal) {
                // alert(message);
                showErrorMessage(message);
            } else {
                // Want to execute error in different manner if modal is true instead of a toast. Do here -
            }

            isFormValid = false;
            return false;
        }
        //CheckBox validation
        if (
            formData[key].validations &&
            formData[key].validations.isRequired &&
            formData[key].isCheckBox &&
            formData[key].value === false
        ) {
            showErrorMessage(`Please select ${formData[key].title}`);
            isFormValid = false;
            return false;
        }

        //TODO 1.1 : STORE OBJECT KEYS IN ARRAY [REQUIRE EMAIL VALIDATION]
        if (validations && validations.isEmail) {
            email_keys.push(key);
        }

        //code for password validation
        if (validations && validations.isPassword) {
            password_keys.push(key);
        }

        //TODO 1.2 : SET ALL REQUIRED CHECK AS TRUE
        if (Object.keys(formData).length === index + 1) {
            isRequiredFieldsValid = true;
        }

        return true;
    });

    // TODO 2 : VALIDATE EMAIL
    if (isRequiredFieldsValid && email_keys.length) {
        email_keys.every((key) => {
            if (!validateEmail(formData[key].value)) {
                const { title } = formData[key];
                const message = `${title ? firstUC(title) : firstUC(key)} is not valid.`;
                if (!options.modal) {
                    // alert(message);
                    showErrorMessage(message);
                } else {
                    // Want to execute error in different manner if modal is true instead of a toast. Do here -
                }
                isFormValid = false;
                return false;
            }

            return true;
        });
    }

    //validate password
    if (isRequiredFieldsValid && password_keys.length) {
        password_keys.every((key) => {
            if (!validatePassword(formData[key].value)) {
                const { title } = formData[key];
                // const message = TextString.password_validation;
                const message = "Password should have minimum length 8";
                if (!options.modal) {
                    showErrorMessage(message);
                }
                isFormValid = false;
                return false;
            }
            return true;
        });
    }
    return isFormValid;
};

const getRespectiveName = (media) => {
    console.log('Respective Media type:-->', media);
    let uri = media.hasOwnProperty('path') ? media.path : media.uri;
    let newName = '';
    if (uri.includes('content:')) {
        // That's mean content Provider name is here
        newName = media.fileName;
    } else {
        let indexOfLastSlash = uri.lastIndexOf('/');
        newName = uri.slice(indexOfLastSlash + 1, uri.length);
    }
    return newName;
};

const getPreparedImageData = (imageMedia) => {
    let tempName = getRespectiveName(imageMedia);
    let tempImage = {
        uri: imageMedia.path,
        name: imageMedia.name,
        fileName: tempName,
        name: tempName,
        path: imageMedia.path,
        type: imageMedia.mime,
        width: imageMedia.width,
        height: imageMedia.height,
        fileSize: imageMedia.size,
        data: imageMedia.path,
    };
    return tempImage;
};

const checkMediaFileSize = (size = 0, limitInMB = 0) => {
    let sizeInMB = size / 1000000;
    if (sizeInMB <= limitInMB) {
        return true;
    } else {
        return false;
    }
};
const checkMediaFileType = (type) => {
    let type_list = ['image/jpeg', 'image/jpg', 'image/png'];
    if (type_list.indexOf(type.toLowerCase()) > -1)
        return true
    else
        return false
};


module.exports = {
    goToNextInput,
    isAndroid,
    isIos,
    isNotEmpty,
    firstUC,
    showErrorMessage,
    validateEmail,
    validateForm,
    generateFormData,
    getPreparedImageData,
    getRespectiveName,
    checkMediaFileSize,
    checkMediaFileType
};
