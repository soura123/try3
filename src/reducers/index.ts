import { combineReducers } from 'redux';
import UserReducer from '../reducers/userReducer'
import UiLoader from '../reducers/Loader';

export default combineReducers(Object.assign({
    UserReducer,
    UiLoader,
}));
