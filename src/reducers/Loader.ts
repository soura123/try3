import { actionConstants } from '../utils/constants/actionConstants' 
import { createReducer } from "../utils/globalFunction";

const initialState = {
    globalLoader: false,
};

export default createReducer(initialState, {
    [actionConstants.show_loader](state, action) {
        return {
            ...state,
            globalLoader: action.globalLoader,
        };
    },
});
