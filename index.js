import 'react-native-gesture-handler'// Because of the Navigation we have to import it at top level
import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';

AppRegistry.registerComponent(appName, () => App);
